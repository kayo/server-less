# Compiler
#COMPILER_NAME ?= arm-none-eabi-

# Base path to build root
BASEPATH := $(or $(subst $(dir $(abspath $(CURDIR)/xyz)),,$(dir $(abspath $(lastword $(MAKEFILE_LIST))))),./)

MKDIR := $(BASEPATH).rules
INCDIR := $(BASEPATH)include
SRCDIR := $(BASEPATH)src
TSTDIR := $(BASEPATH)test
LDDIR := $(BASEPATH)ld
LIBSDIR := $(BASEPATH).libs

-include config.mk

# Common
include $(MKDIR)/macro.mk
include $(MKDIR)/option.mk
include $(MKDIR)/build.mk
include $(MKDIR)/stalin.mk

# Libraries
TARGET.LIBS += libedht
libedht.INHERIT := software
libedht.SRCS = $(addprefix $(SRCDIR)/,\
  bcoder.c \
  krpc.c)

define TST_RULES
TARGET.LIBS += test/lib$(1)
test/lib$(1).INHERIT += libedht
test/lib$(1).SRCS += $(addprefix $(TSTDIR)/,\
  $(1).c)

TARGET.BINS += test/$(1)
test/$(1).INHERIT += libedht test/lib$(1)
test/$(1).DEPLIBS += libedht
test/$(1).DEPLIBS* += test/lib$(1)

test: test.$(1)
test.$(1): run.test/$(1)
endef

test:
TARGET.TSTS += bcoder krpc

# Application
software.INHERIT += stalin
software.CDIRS += $(INCDIR)
software.LDDIRS += $(LDDIR)
software.GDBOPTS += -ex 'set pagination off'
#software.CCPIPE := CCPRE_RULE CPP_RULE OBJ_RULE

$(call use_toolchain,software,$(COMPILER_NAME))

# Provide rules
$(call ADDRULES,\
OPT_RULES:TARGET.OPTS\
TST_RULES:TARGET.TSTS\
LIB_RULES:TARGET.LIBS\
BIN_RULES:TARGET.BINS)
