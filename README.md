Server-less IoT communication library {#index}
=====================================

This library designed in platform independed way and haven't extra dependencies

Currently it includes the next parts:

* The bencoding encoder/decoder
* The KRPC protocol implementation
* The DHT service handler
