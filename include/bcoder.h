#ifndef __BCODER_H__
#define __BCODER_H__
/**
 * @defgroup bcoder Bencoding codec
 * @brief The embedded implementation of bencoding codec
 *
 * The bencoding was introduced in the BitTorrent Protocol Specification http://bittorrent.org/beps/bep_0003.html.
 * This is a light and fast implementation of the bencoding decoder (parser) and encoder (formatter).
 *
 * @{
 */

#include <stddef.h>

/**
 * @brief The bencoding entity type
 */
typedef enum {
  /**
   * @brief The invalid entity
   */
  bc_ent_bad,
  /**
   * @brief The string entity
   */
  bc_ent_str,
  /**
   * @brief The integer entity
   */
  bc_ent_int,
  /**
   * @brief The list entity
   */
  bc_ent_lst,
  /**
   * @brief The dictionary entity
   */
  bc_ent_dct,
} bc_ent_t;

/**
 * @defgroup bdecoder Bencoding decoder
 * @brief The bencoding decoder functions
 *
 * @{
 */

/**
 * @brief Get unsigned integer up to terminating character
 *
 * @param[in] ptr The pointer to beginning of source buffer
 * @param[in] end The pointer to end of source buffer
 * @param[in] chr The terminating character
 * @param[out] val The pointer to value
 * @return The pointer to position immediately after terminating character or @p NULL when error occurred
 *
 * This function tries to decode unsigned integer from @p ptr to @p end until the @p chr has seen.
 * If @p val is @p NULL then function skips the integer without parsing it.
 *
 * This is a helper function.
 */
const char *
bc_get_uint_to(const char *ptr,
               const char *end,
               char chr,
               unsigned *val);

/**
 * @brief Get entity type
 *
 * @param[in] ptr The pointer to beginning of source buffer
 * @param[in] end The pointer to end of source buffer
 * @return The type of the found entity or @p bc_ent_bad when no entity found
 *
 * This function tries to determine the bencoding entity type at @p ptr.
 */
bc_ent_t
bc_get_ent(const char *ptr,
           const char *end);

/**
 * @brief Get the string entity
 *
 * @param[in] ptr The pointer to beginning of source buffer
 * @param[in] end The pointer to end of source buffer
 * @param[out] val The pointer to string to return
 * @return The pointer to position immediately after string entity (and so the pointer to the end of decoded string) or @p NULL when error occurred
 *
 * This function tries to decode string entity from @p ptr to @p end.
 * If @p val is @p NULL then function skips the string entity without parsing it.
 */
const char *
bc_get_str(const char *ptr,
           const char *end,
           const char **val);

/**
 * @brief Get the integer entity
 *
 * @param[in] ptr The pointer to beginning of source buffer
 * @param[in] end The pointer to end of source buffer
 * @param[out] val The pointer to integer to return
 * @return The pointer to position immediately after integer entity or @p NULL when error occurred
 *
 * This function tries to decode integer entity from @p ptr to @p end.
 * If @p val is @p NULL then function skips the integer entity without parsing it.
 */
const char*
bc_get_int(const char *ptr,
           const char *end,
           int *val);

/**
 * @brief The callback function to get the list element entity
 *
 * @param[in] ptr The pointer to beginning of source buffer
 * @param[in] end The pointer to end of source buffer
 * @param[in] env The pointer to the environment of the callback function
 * @return The pointer to position immediately after list element entity or @p NULL when error occurred
 *
 * This callback function must try to decode list element entity from @p ptr to @p end.
 */
typedef const char*
bc_lst_get_f(const char *ptr,
             const char *end,
             void *env);

/**
 * @brief Get the list entity
 *
 * @param[in] ptr The pointer to beginning of source buffer
 * @param[in] end The pointer to end of source buffer
 * @param[in] get The callback function which will be called for each element of the list
 * @param[in] env The pointer to the environment for the callback function
 * @return The pointer to position immediately after the list entity or @p NULL when error occurred
 *
 * This function tries to decode list entity from @p ptr to @p end.
 * If @p get is @p NULL then function skips the list entity without parsing it.
 */
const char*
bc_get_lst(const char *ptr,
           const char *end,
           bc_lst_get_f *get,
           void *env);

/**
 * @brief The callback function to get the dictionary element entity
 *
 * @param[in] key The pointer to beginning of the key
 * @param[in] ptr The pointer to beginning of source buffer and end of the key
 * @param[in] end The pointer to end of source buffer
 * @param[in] env The pointer to the environment of the callback function
 * @return The pointer to position immediately after dictionary element entity or @p NULL when error occurred
 *
 * This callback function must try to decode dictionary element entity from @p ptr to @p end.
 * The @p key is a beginning point of string which ends on @p ptr.
 */
typedef const char*
bc_dct_get_f(const char *key,
             const char *ptr,
             const char *end,
             void *env);

/**
 * @brief Get the dictionary entity
 *
 * @param[in] ptr The pointer to beginning of source buffer
 * @param[in] end The pointer to end of source buffer
 * @param[in] get The callback function which will be called for each element of the dictionary
 * @param[in] env The pointer to the environment for the callback function
 * @return The pointer to position immediately after the dictionary entity or @p NULL when error occurred
 *
 * This function tries to decode dictionary entity from @p ptr to @p end.
 * If @p get is @p NULL then function skips the dictionary entity without parsing it.
 */
const char*
bc_get_dct(const char *ptr,
           const char *end,
           bc_dct_get_f *get,
           void *env);

/**
 * @brief Skip any valid entity
 *
 * @param[in] ptr The pointer to beginning of source buffer
 * @param[in] end The pointer to end of source buffer
 * @return The pointer to position immediately after the skipped entity or @p NULL when error occurred
 *
 * This function tries to skip any valid entity from @p ptr to @p end.
 */
const char*
bc_get_any(const char *ptr,
           const char *end);

/**
 * @brief Get generic entity
 *
 * @param[in] ent The entity name (str, int, lst, dct or any)
 * @param[in] ptr The pointer to beginning of source buffer
 * @param[in] end The pointer to end of source buffer
 * @param ... The additional arguments
 * @return The pointer to position immediately after the decoded entity or @p NULL when error occurred
 *
 * This is a helper macro.
 */
#define bc_get(ent, ptr, end, ...)       \
  bc_get_##ent(ptr, end, ##__VA_ARGS__)

/**
 * @}
 */

/**
 * @defgroup bencoder Bencoding encoder
 * @brief The bencoding encoder functions
 *
 * @{
 */

/**
 * @brief Put unsigned integer and terminating character
 *
 * @param[in] ptr The pointer to beginning of target buffer
 * @param[in] end The pointer to end of target buffer
 * @param[in] chr The terminating character
 * @param[out] val The value to encode
 * @return The pointer to position immediately after terminating character or @p NULL when @p end position reached
 *
 * This function tries to encode unsigned integer from @p ptr to @p end.
 *
 * This is a helper function.
 */
char *
bc_put_uint_to(char *ptr,
               char *end,
               char chr,
               unsigned val);

/**
 * @brief Put the string entity
 *
 * @param[in] ptr The pointer to beginning of target buffer
 * @param[in] end The pointer to end of target buffer
 * @param[in] str The pointer to beginning of string to insert
 * @param[in] out The pointer to end of string to insert
 * @return The pointer to position immediately after inserted string entity or @p NULL when @p end position reached
 */
char *
bc_put_str(char *ptr,
           char *end,
           const char *str,
           const char *out);

/**
 * @brief Put the null-terminated string entity
 *
 * @param[in] ptr The pointer to beginning of target buffer
 * @param[in] end The pointer to end of target buffer
 * @param[in] str The null-terminated string to insert
 * @return The pointer to position immediately after inserted string entity or @p NULL when @p end position reached
 */
#define bc_put_nstr(ptr, end, str) ({  \
      bc_put_str(ptr, end, str,        \
                 (str) + strlen(str)); \
    })

/**
 * @brief Put the constant string entity
 *
 * @param[in] ptr The pointer to beginning of target buffer
 * @param[in] end The pointer to end of target buffer
 * @param[in] str The constant string to insert
 * @return The pointer to position immediately after inserted string entity or @p NULL when @p end position reached
 */
#define bc_put_cstr(ptr, end, str) ({     \
      const char __cstr__[] = (str);      \
      bc_put_str(ptr, end, __cstr__,      \
                 __cstr__ +               \
                 sizeof(__cstr__) - 1);   \
    })

/**
 * @brief Put the statically sized buffer
 *
 * @param[in] ptr The pointer to beginning of target buffer
 * @param[in] end The pointer to end of target buffer
 * @param[in] buf The pointer to source buffer
 * @return The pointer to position immediately after inserted string entity or @p NULL when @p end position reached
 */
#define bc_put_buf(ptr, end, buf) ({   \
      bc_put_str(ptr, end, buf,        \
                 (buf) + sizeof(buf)); \
    })

/**
 * @brief Put the string entity
 *
 * @param[in] ptr The pointer to beginning of target buffer
 * @param[in] end The pointer to end of target buffer
 * @param[in] val The integer value to insert
 * @return The pointer to position immediately after inserted integer entity or @p NULL when @p end position reached
 */
char *
bc_put_int(char *ptr,
           char *end,
           int val);

/**
 * @brief The callback function to put the sequence element entity
 *
 * @param[in] ptr The pointer to beginning of target buffer
 * @param[in] end The pointer to end of target buffer
 * @param[in] env The pointer to the environment of the callback function
 * @return The pointer to position immediately after inserted sequence element entity or @p ptr when no elements to insert or @p NULL when @p end position reached
 *
 * This callback function must try to encode sequence element entity from @p ptr to @p end.
 * If no elements to insert then @p ptr must be returned.
 */
typedef char *
bc_seq_put_f(char *ptr,
             char *end,
             void *env);

/**
 * @brief Put the sequence entity
 *
 * @param[in] ptr The pointer to beginning of target buffer
 * @param[in] end The pointer to end of target buffer
 * @param[in] chr The initiating character for sequence
 * @param[in] put The callback function which will be called to put each element of sequence
 * @param[in] env The pointer to the environment for the callback function
 * @return The pointer to position immediately after inserted sequence entity or @p NULL when @p end position reached
 */
char *bc_put_seq(char *ptr,
                 char *end,
                 char chr,
                 bc_seq_put_f *put,
                 void *env);

/**
 * @brief The callback function to put the list element entity
 *
 * @param[in] ptr The pointer to beginning of target buffer
 * @param[in] end The pointer to end of target buffer
 * @param[in] env The pointer to the environment of the callback function
 * @return The pointer to position immediately after inserted list element entity or @p ptr when no elements to insert or @p NULL when @p end position reached
 *
 * This callback function must try to encode list element entity from @p ptr to @p end.
 * If no elements to insert then @p ptr must be returned.
 */
typedef bc_seq_put_f bc_lst_put_f;

/**
 * @brief Put the list entity
 *
 * @param[in] ptr The pointer to beginning of target buffer
 * @param[in] end The pointer to end of target buffer
 * @param[in] put The callback function which will be called to put each element of the list
 * @param[in] env The pointer to the environment for the callback function
 * @return The pointer to position immediately after inserted list entity or @p NULL when @p end position reached
 */
static inline char *
bc_put_lst(char *ptr,
           char *end,
           bc_lst_put_f *put,
           void *env) {
  return bc_put_seq(ptr, end, 'l', put, env);
}

/**
 * @brief The callback function to put the dictionary element entity
 *
 * @param[in] ptr The pointer to beginning of target buffer
 * @param[in] end The pointer to end of target buffer
 * @param[in] env The pointer to the environment of the callback function
 * @return The pointer to position immediately after inserted dictionary element entity or @p ptr when no elements to insert or @p NULL when @p end position reached
 *
 * This callback function must try to encode dictionary element entity from @p ptr to @p end.
 * If no elements to insert then @p ptr must be returned.
 */
typedef bc_seq_put_f bc_dct_put_f;

/**
 * @brief Put generic dictionary element entity
 *
 * @param[in] key The constant string key
 * @param[in] ent The entity name (str, int, lst, dct or any)
 * @param[in] ptr The pointer to beginning of target buffer
 * @param[in] end The pointer to end of target buffer
 * @param ... The additional arguments
 * @return The pointer to position immediately after inserted entity or @p NULL when @p end position reached
 *
 * This is a helper macro.
 */
#define bc_dct_put(key, ent, ptr, end, ...) ({  \
      /* encode key string entity */            \
      char *__ptr__ =                           \
        bc_put(cstr, ptr, end, key);            \
      if (__ptr__ != NULL) {                    \
        /* encode value entity */               \
        __ptr__ = bc_put(ent, __ptr__, end,     \
                         ##__VA_ARGS__);        \
      }                                         \
      __ptr__;                                  \
    })

/**
 * @brief Put the dictionary entity
 *
 * @param[in] ptr The pointer to beginning of target buffer
 * @param[in] end The pointer to end of target buffer
 * @param[in] put The callback function which will be called to put each element of the dictionary
 * @param[in] env The pointer to the environment for the callback function
 * @return The pointer to position immediately after inserted dictionary entity or @p NULL when @p end position reached
 */
static inline char *
bc_put_dct(char *ptr,
           char *end,
           bc_dct_put_f *put,
           void *env) {
  return bc_put_seq(ptr, end, 'd', put, env);
}

/**
 * @brief Put generic entity
 *
 * @param[in] ent The entity name (str, int, lst, dct or any)
 * @param[in] ptr The pointer to beginning of target buffer
 * @param[in] end The pointer to end of target buffer
 * @param ... The additional arguments
 * @return The pointer to position immediately after inserted entity or @p NULL when @p end position reached
 *
 * This is a helper macro.
 */
#define bc_put(ent, ptr, end, ...)       \
  bc_put_##ent(ptr, end, ##__VA_ARGS__)

/**
 * @}
 */

/**
 * @}
 */
#endif /* __BCODER_H__ */
