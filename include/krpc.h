#ifndef __KRPC_H__
#define __KRPC_H__
/**
 * @defgroup krpc KRPC protocol
 * @brief The embedded implementation of KRPC protocol
 *
 * The KRPC was introduced in the DHT Protocol Specification http://bittorrent.org/beps/bep_0005.html
 * This is a light and fast implementation of the KRPC protocol handler.
 *
 * @{
 */

#include "bcoder.h"

/**
 * @brief Get the number of elements into the static array
 *
 * @param[in] ary The source array
 * @return The number of elements
 */
#define krpc_numof(ary) (sizeof(ary) / sizeof(ary[0]))

/**
 * @brief Get the end of the static array
 *
 * @param[in] ary The source array
 * @return The pointer to end of array
 */
#define krpc_endof(ary) ((ary) + krpc_numof(ary))

/**
 * @brief The KRPC message type
 */
typedef enum {
  /**
   * @brief Query message
   */
  krpc_msg_query = 'q',
  /**
   * @brief Response message
   */
  krpc_msg_response = 'r',
  /**
   * @brief Error message
   */
  krpc_msg_error = 'e',
} krpc_msg_t;

/**
 * @brief The KRPC error type
 */
typedef enum {
  /**
   * @brief Generic Error
   */
  krpc_err_generic = 201,
  /**
   * @brief Server Error
   */
  krpc_err_server = 202,
  /**
   * @brief Protocol Error
   *
   * @i malformed packet
   * @i invalid arguments
   * @i bad token
   */
  krpc_err_proto = 203,
  /**
   * @brief Method unknown
   */
  krpc_err_method = 204,
} krpc_err_t;

/**
 * @brief The KRPC string
 */
typedef struct {
  /**
   * @brief The pointer to beginning of string data
   */
  const char *ptr;
  /**
   * @brief The pointer to end of string data
   */
  const char *end;
} krpc_str_t;

/**
 * @brief Static init KRPC string
 * 
 * @param[in] ptr The pointer to beginning of string
 * @param[in] end The pointer to end of string
 * @return The static string initializer
 */
#define krpc_str(ptr, end) { ptr, end }

/**
 * @brief Static init KRPC string
 *
 * @param[in] ptr The constant string literal
 * @return The static string initializer
 */
#define krpc_cstr(str) krpc_str(str, krpc_endof(str) - 1)

/**
 * @brief Static init KRPC string
 *
 * @param[in] ptr The null-terminated string
 * @return The static string initializer
 */
#define krpc_nstr(str) krpc_str(str, (str) + strlen(str))

/**
 * @brief Set KRPC string value
 *
 * @param[in] str The pointer to KRPC string
 * @param[in] ptr The pointer to beginning of string
 * @param[in] end The pointer to end of string
 */
static inline void
krpc_set_str(krpc_str_t *str,
             const char *ptr,
             const char *end) {
  str->ptr = ptr;
  str->end = end;
}

/**
 * @brief Set KRPC string value
 *
 * @param[in] str The pointer to KRPC string
 * @param[in] ptr The constant string literal
 */
#define krpc_set_cstr(str, ptr) \
  krpc_set_str(str, ptr, krpc_endof(ptr) - 1)

/**
 * @brief Set KRPC string value
 *
 * @param[in] str The pointer to KRPC string
 * @param[in] ptr The nil-terminated string
 */
#define krpc_set_nstr(str, ptr) \
  krpc_set_str(str, (ptr) + strlen(ptr))

/**
 * @brief The KRPC data type
 */
typedef struct {
  /**
   * @brief The message type
   */
  krpc_msg_t type;
  /**
   * @brief The transaction ID
   */
  krpc_str_t tid;
  union {
    struct {
      /**
       * @brief The query string
       */
      krpc_str_t query;
      /**
       * @brief The environment for arguments callback
       */
      void *env;
    };
    struct {
      /**
       * @brief The error message
       */
      krpc_str_t msg;
      /**
       * @brief The error code
       */
      unsigned code;
    };
  };
} krpc_data_t;

/**
 * @brief Instantiate KRPC data object for decoding
 *
 * @param[in] name The name of variable
 * @param[in] env_ The pointer to environment for callback
 * @return The initialized data object in stack
 */
#define krpc_data_none(name, env_) \
  krpc_data_t name = {             \
    .type = krpc_msg_error,        \
    .tid = { NULL, NULL },         \
    .env = env_,                   \
    .query = { NULL, NULL },       \
  }

/**
 * @brief Instantiate KRPC data object for encoding
 *
 * @param[in] name The name of variable
 * @param[in] type_ The type of message
 * @param[in] tid_ The transaction ID
 * @param[in] env_ The pointer to environment for callback
 * @return The initialized data object in stack
 */
#define krpc_data_message(name, type_, tid_, env_) \
  krpc_data_t name = {                             \
    .type = type_,                                 \
    .tid = tid_,                                   \
    .env = env_,                                   \
    .query = { NULL, NULL },                       \
  }

/**
 * @brief Instantiate KRPC data object for encoding query
 *
 * @param[in] name The name of variable
 * @param[in] tid_ The transaction ID
 * @param[in] query_ The constant string of query type
 * @param[in] env_ The pointer to environment for callback
 * @return The initialized data object in stack
 */
#define krpc_data_query(name, tid_, query_, env_) \
  krpc_data_t name = {                            \
    .type = krpc_msg_query,                       \
    .tid = tid_,                                  \
    .env = env_,                                  \
    .query = krpc_cstr(query_),                   \
  }

/**
 * @brief Instantiate KRPC data object for encoding response
 *
 * @param[in] name The name of variable
 * @param[in] tid_ The transaction ID
 * @return The initialized data object in stack
 */
#define krpc_data_response(name, tid_, env_)    \
  krpc_data_t name = {                          \
    .type = krpc_msg_response,                  \
    .tid = tid_,                                \
    .env = env_,                                \
  }

/**
 * @brief Instantiate KRPC data object for encoding error
 *
 * @param[in] name The name of variable
 * @param[in] tid_ The transaction ID
 * @param[in] code_ The error code
 * @param[in] msg_ The error message
 * @return The initialized data object in stack
 */
#define krpc_data_error(name, tid_, code_, msg_) \
  krpc_data_t name = {                           \
    .type = krpc_msg_error,                      \
    .tid = tid_,                                 \
    .code = code_,                               \
    .msg = msg_,                                 \
  }

/**
 * @brief Instantiate KRPC data object for encoding or decoding
 *
 * @param[in] msg The type of message (none, query, response or error)
 * @param[in] name The name of variable
 * @param[in] ... The other arguments
 * @return The initialized data object in stack
 */
#define krpc_data(msg, name, ...)               \
  krpc_data_##msg(name, ##__VA_ARGS__)

/**
 * @brief Decode KRPC message
 *
 * @param[in] ptr The pointer to beginning of source buffer
 * @param[in] end The pointer to end of source buffer
 * @param[in] msg The pointer to message data object
 * @param[in] get The callback function for decoding arguments or results
 * @return The pointer to position immediately after decoded entity or @p NULL when decoding error occurred
 */
const char *
krpc_get(const char *ptr,
         const char *end,
         krpc_data_t *msg,
         bc_dct_get_f *get);

/**
 * @brief Encode KRPC query
 *
 * @param[in] ptr The pointer to beginning of target buffer
 * @param[in] end The pointer to end of target buffer
 * @param[in] msg The pointer to message data object
 * @param[in] put The callback function for encoding arguments or results
 * @return The pointer to position immediately after inserted query entity or @p NULL when @p end position reached
 */
char *
krpc_put(char *ptr,
         char *end,
         const krpc_data_t *msg,
         bc_dct_put_f *put);

/**
 * @brief Get the string entity as KRPC string
 *
 * @param[in] ptr The pointer to beginning of source buffer
 * @param[in] end The pointer to end of source buffer
 * @param[out] val The pointer to KRPC string to return
 * @return The pointer to position immediately after string entity (and so the pointer to the end of decoded string) or @p NULL when error occurred
 *
 * This function tries to decode string entity from @p ptr to @p end.
 * If @p val is @p NULL then function skips the string entity without parsing it.
 */
static inline const char*
bc_get_krpc_str(const char *ptr,
                const char *end,
                krpc_str_t *val) {
  if (val != NULL) {
    return val->end = bc_get(str, ptr, end, &val->ptr);
  }
  return bc_get(str, ptr, end, NULL);
}

/**
 * @brief Put KRPC string as the string entity
 *
 * @param[in] ptr The pointer to beginning of target buffer
 * @param[in] end The pointer to end of target buffer
 * @param[in] str The pointer to KRPC string to insert
 * @return The pointer to position immediately after inserted string entity or @p NULL when @p end position reached
 */
static inline char *
bc_put_krpc_str(char *ptr,
                char *end,
                const krpc_str_t *val) {
  return bc_put(str, ptr, end, val->ptr, val->end);
}

/**
 * @}
 */
#endif /* __KRPC_H__ */
