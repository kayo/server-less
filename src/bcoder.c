#include <stddef.h>
#include "bcoder.h"

/*
 * Decoder (parser) functions
 */

const char *
bc_get_uint_to(const char *ptr,
               const char *end,
               char chr,
               unsigned *val) {
  if (ptr >= end ||
      *ptr == chr || /* avoid to parse empty (no digits) */
      (ptr + 1 < end &&
       *ptr == '0' && /* avoid to parse sequence of zeros */
       ptr[1] != chr)) {
    return NULL;
  }
  
  const char *str = ptr;
  
  for (; str < end; str ++) {
    /* skip digits up to terminating character */
    if (*str >= '0' &&
        *str <= '9') {
      continue;
    } else if (*str == chr) {
      goto parse;
    } else {
      break;
    }
  }
  
  return NULL;
  
 parse:
  {
    /* parse the number only when we have pointer to value */
    if (val != NULL) {
      for (*val = 0; ptr < str; ptr ++) {
        *val = *val * 10 + (*ptr - '0');
      }
    }
    
    return str + 1;
  }
}

bc_ent_t bc_get_ent(const char *ptr,
                    const char *end) {
  if (ptr < end) {
    switch (*ptr) {
    case 'i': return bc_ent_int;
    case 'l': return bc_ent_lst;
    case 'd': return bc_ent_dct;
    }
    if (NULL != bc_get_uint_to(ptr, end, ':', NULL)) {
      return bc_ent_str;
    }
  }
  return bc_ent_bad;
}

const char *
bc_get_str(const char *ptr,
           const char *end,
           const char **val) {
  unsigned len;
  
  ptr = bc_get_uint_to(ptr, end, ':', &len);

  if (ptr == NULL) {
    return NULL;
  }
  
  if (val != NULL) {
    *val = ptr;
  }
  
  return ptr + len;
}

const char *
bc_get_int(const char *ptr,
           const char *end,
           int *val) {
  if (ptr >= end ||
      *ptr != 'i') {
    return NULL;
  }
    
  char neg = ptr[1] == '-';
  
  if (neg) {
    if (ptr[2] == '0') {
      /* -0 is invalid */
      return NULL;
    }

    ptr ++;
  }
  
  ptr = bc_get_uint_to(ptr + 1, end, 'e', (unsigned*)val);

  if (ptr == NULL) {
    return NULL;
  }
  
  if (val != NULL && neg) {
    *val = - *val;
  }
  
  return ptr;
}

static const char*
lst_get_any(const char *ptr,
            const char *end,
            void *env) {
  (void)env;
  
  return bc_get_any(ptr, end);
}

const char*
bc_get_lst(const char *ptr,
           const char *end,
           bc_lst_get_f *get,
           void *env) {
  if (ptr >= end ||
      *ptr != 'l') {
    return NULL;
  }

  ptr ++;
  
  if (get == NULL) {
    get = lst_get_any;
  }

  for (; ptr < end; ) {
    if (*ptr == 'e') {
      return ptr + 1;
    }
    
    ptr = get(ptr, end, env);
    
    if (ptr == NULL) {
      break;
    }
  }
  
  return NULL;
}

static const char*
dct_get_any(const char *key,
            const char *ptr,
            const char *end,
            void *env) {
  (void)key;
  (void)env;
  
  return bc_get_any(ptr, end);
}

const char*
bc_get_dct(const char *ptr,
           const char *end,
           bc_dct_get_f *get,
           void *env) {
  if (ptr >= end ||
      *ptr != 'd') {
    return NULL;
  }

  ptr ++;
  
  if (get == NULL) {
    get = dct_get_any;
  }
  
  for (; ptr < end; ) {
    const char *key;
    
    if (*ptr == 'e') {
      return ptr + 1;
    }
    
    ptr = bc_get_str(ptr, end, &key);
    
    if (ptr == NULL) {
      break;
    }
    
    ptr = get(key, ptr, end, env);
    
    if (ptr == NULL) {
      break;
    }
  }
  
  return NULL;
}

const char*
bc_get_any(const char *ptr,
           const char *end) {
  switch (bc_get_ent(ptr, end)) {
  case bc_ent_str: return bc_get_str(ptr, end, NULL);
  case bc_ent_int: return bc_get_int(ptr, end, NULL);
  case bc_ent_lst: return bc_get_lst(ptr, end, NULL, NULL);
  case bc_ent_dct: return bc_get_dct(ptr, end, NULL, NULL);
  case bc_ent_bad: break;
  }
  return NULL;
}

/*
 * Encoder (formatter) functions
 */

char *
bc_put_uint_to(char *ptr,
               char *end,
               char chr,
               unsigned val) {
  unsigned d = 1;
  unsigned c = 10;
  
  /* estimating the number of decimal digits */
  for (; val >= c; c *= 10, d++);
  
  if (ptr + d + 1 > end) {
    /* not enough space in target buffer */
    return NULL;
  }
  
  for (end = ptr + d - 1; end >= ptr; end --) {
    *end = '0' + (val % 10);
    val /= 10;
  }
  
  end = ptr + d;
  *end = chr;
  
  return end + 1;
}

char *bc_put_str(char *ptr,
                 char *end,
                 const char *str,
                 const char *out) {
  ptr = bc_put_uint_to(ptr, end, ':', out - str);

  if (ptr == NULL) {
    return NULL;
  }

  if (out - str > end - ptr) {
    return NULL;
  }

  for (; str < out; ) {
    *ptr ++ = *str ++;
  }

  return ptr;
}

char *bc_put_int(char *ptr,
                 char *end,
                 int val) {
  if (ptr + 3 > end) {
    /* not enough space in target buffer */
    return NULL;
  }
  
  *ptr ++ = 'i';
  
  if (ptr + 2 < end &&
      val < 0) {
    *ptr ++ = '-';
    val = - val;
  }
  
  return bc_put_uint_to(ptr, end, 'e', val);
}

char *bc_put_seq(char *ptr,
                 char *end,
                 char chr,
                 bc_seq_put_f *put,
                 void *env) {
  if (ptr + 2 > end) {
    /* not enough space in target buffer */
    return NULL;
  }

  *ptr ++ = chr;

  for (; ; ) {
    char *_ptr = ptr;
    
    ptr = put(ptr, end, env);
    
    if (ptr == NULL) {
      return NULL;
    } else if (ptr == _ptr) {
      break;
    }
  }

  if (ptr >= end) {
    /* not enough space in target buffer */
    return NULL;
  }

  *ptr ++ = 'e';
  
  return ptr;
}
