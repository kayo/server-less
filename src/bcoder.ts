/**
 * Bencoding codec
 *
 * The embedded implementation of bencoding codec.
 *
 * The bencoding was introduced in the BitTorrent Protocol Specification http://bittorrent.org/beps/bep_0003.html.
 * This is a light and fast implementation of the bencoding decoder (parser) and encoder (formatter).
 */

/**
 * The reference type which can be used like a C-pointer
 */
export type bc_ref<val_t> = [any, string | number];

/**
 * Make reference to value
 *
 * @param obj The target object or array
 * @param key The name of referenced data field
 * @return The reference
 */
export function bc_ref<val_t>(obj: any = [], key: string | number = 0): bc_ref<val_t> {
  return [obj, key];
}

/**
 * Get referenced value
 *
 * @param ref The reference
 * @return The referenced value
 */
export function bc_ref_get<val_t>(ref: bc_ref<val_t>): val_t {
  return ref[0][ref[1]];
}

/**
 * Set referenced value
 *
 * @param ref The reference
 * @param val The new value
 */
export function bc_ref_set<val_t>(ref: bc_ref<val_t>, val: val_t): void {
  ref[0][ref[1]] = val;
}

/**
 * The bencoding entity type
 */
export const enum bc_ent {
  /**
   * The invalid entity
   */
  bad,
  /**
   * The string entity
   */
  str,
  /**
   * The integer entity
   */
  int,
  /**
   * The list entity
   */
  lst,
  /**
   * The dictionary entity
   */
  dct,
};

/**
 * Get unsigned integer up to terminating character
 *
 * @param buf The source buffer
 * @param chr The terminating character
 * @param val The reference to value or `null` to skip parsing
 * @return The buffer from position immediately after terminating character and integer value or `null` when error occurred
 *
 * This function tries to decode unsigned integer from `ptr` until the `chr` has seen.
 *
 * This is a helper function.
 */
export function bc_get_uint_to(buf: Buffer, chr: number, val?: bc_ref<number>): Buffer | null {
  if (buf.length < 1 ||
    buf.readUInt8(0) == chr || /* avoid to parse empty (no digits) */
    (buf.length > 1 &&
      buf.readUInt8(0) == 0x30/*0*/ && /* avoid to parse sequence of zeros */
      buf.readUInt8(1) != chr)) {
    return null;
  }

  let i = 0;

  if (val) {
    bc_ref_set(val, 0);
  }

  for (; i < buf.length; i++) {
    const c = buf.readUInt8(i);

    if (c < 0x30 || 0x39 < c) {
      if (c == chr) {
        return buf.slice(i + 1);
      }

      break;
    }

    if (val) {
      bc_ref_set(val, bc_ref_get<number>(val) * 10 + (c - 0x30));
    }
  }

  return null;
}

/**
 * Get entity type
 *
 * @param buf The source buffer
 * @return The type of the found entity or `bc_ent.bad` when no entity found
 *
 * This function tries to determine the bencoding entity type at beginning of source buffer.
 */
export function bc_get_ent(buf: Buffer): bc_ent {
  if (buf.length > 0) {
    switch (buf.readUInt8(0)) {
      case 0x69/*i*/: return bc_ent.int;
      case 0x6c/*l*/: return bc_ent.lst;
      case 0x64/*d*/: return bc_ent.dct;
    }

    if (null != bc_get_uint_to(buf, 0x3a/*:*/)) {
      return bc_ent.str;
    }
  }

  return bc_ent.bad;
}

/**
 * Get the string entity
 *
 * @param buf The source buffer
 * @param val The reference to string or `null` to skip parsing
 * @return The buffer from position immediately after string entity with content of decoded string or `null` when error occurred
 *
 * This function tries to decode string entity from beginning of `buf`.
 */
export function bc_get_str(buf: Buffer, val?: bc_ref<Buffer>): Buffer | null {
  const len = bc_ref<number>();
  const res = bc_get_uint_to(buf, 0x3a/*:*/, len);

  if (res != null) {
    if (val) {
      bc_ref_set(val, res.slice(0, bc_ref_get<number>(len)));
    }
    return res.slice(bc_ref_get<number>(len));
  }

  return null;
}

/**
 * Get the integer entity
 *
 * @param buf The source buffer
 * @param val The pointer to value or `null` to skip parsing
 * @return The buffer from position immediately after integer entity with decoded integer entity or `null` when error occurred
 *
 * This function tries to decode integer entity from beginning of `buf`.
 */
export function bc_get_int(buf: Buffer, val?: bc_ref<number>): Buffer | null {
  if (buf.length < 1 ||
    buf.readUInt8(0) != 0x69/*i*/) {
    return null;
  }

  let neg = buf.readUInt8(1) == 0x2d/*-*/;

  if (neg) {
    if (buf.readUInt8(2) == 0x30/*0*/) {
      /* -0 is invalid */
      return null;
    }

    buf = buf.slice(2);
  } else {
    buf = buf.slice(1);
  }

  const res = bc_get_uint_to(buf, 0x65/*e*/, val);

  if (res == null) {
    return null;
  }

  if (val && neg) {
    bc_ref_set(val, - bc_ref_get(val));
  }

  return res;
}

export type bc_lst_get<env_t> =
  /**
   * The callback function to get the list element entity
   *
   * @param buf The source buffer
   * @param env The environment of the callback function
   * @return The buffer from position immediately after list element entity or `null` when error occurred
   *
   * This callback function must try to decode list element entity from beginning of `buf`.
   */
  (buf: Buffer, env?: env_t) => Buffer | null;

function lst_get_any(buf: Buffer, env: any): Buffer | null {
  return bc_get_any(buf);
}

/**
 * @brief Get the list entity
 *
 * @param buf The source buffer
 * @param get The callback function which will be called for each element of the list
 * @param env The environment for the callback function
 * @return The buffer from position immediately after the list entity or `null` when error occurred
 *
 * This function tries to decode list entity from beginning of `buf`.
 */
export function bc_get_lst<env_t>(buf: Buffer, get?: bc_lst_get<env_t>, env?: env_t): Buffer | null {
  if (buf.length < 1 ||
    buf.readUInt8(0) != 0x6c/*l*/) {
    return null;
  }

  buf = buf.slice(1);

  if (get == null) {
    get = lst_get_any;
  }

  for (; buf.length > 0;) {
    if (buf.readUInt8(0) == 0x65/*e*/) {
      return buf.slice(1);
    }

    const res = get(buf, env);

    if (res == null) {
      break;
    }

    buf = res;
  }

  return null;
}

export type bc_dct_get<env_t> =
  /**
   * The callback function to get the dictionary element entity
   *
   * @param key The key string
   * @param buf The source buffer
   * @param env The environment of the callback function
   * @return The buffer from position immediately after dictionary element entity or `null` when error occurred
   *
   * This callback function must try to decode dictionary element entity from beginning of `buf`.
   */
  (key: Buffer, buf: Buffer, env?: env_t) => Buffer | null;

function dct_get_any(key: Buffer, buf: Buffer, env: any): Buffer | null {
  return bc_get_any(buf);
}

/**
 * Get the dictionary entity
 *
 * @param buf The source buffer
 * @param get The callback function which will be called for each element of the dictionary
 * @param env The environment for the callback function
 * @return The buffer from position immediately after the dictionary entity or `null` when error occurred
 *
 * This function tries to decode dictionary entity from beginning of `buf`.
 * If `get` is `null` then function skips the dictionary entity without parsing it.
 */
export function bc_get_dct<env_t>(buf: Buffer, get?: bc_dct_get<env_t>, env?: env_t): Buffer | null {
  if (buf.length < 1 ||
    buf.readUInt8(0) != 0x64/*d*/) {
    return null;
  }

  buf = buf.slice(1);

  if (get == null) {
    get = dct_get_any;
  }

  for (; buf.length > 0;) {
    if (buf.readUInt8(0) == 0x65/*e*/) {
      return buf.slice(1);
    }

    const key = bc_ref<Buffer>();
    let res = bc_get_str(buf, key);

    if (res == null) {
      break;
    }

    res = get(bc_ref_get<Buffer>(key), res, env);

    if (res == null) {
      break;
    }

    buf = res;
  }

  return null;
}

/**
 * Skip any valid entity
 *
 * @param buf The source buffer
 * @return The buffer from position immediately after the skipped entity or `null` when error occurred
 *
 * This function tries to skip any valid entity from beginning of `buf`.
 */
export function bc_get_any(buf: Buffer): Buffer | null {
  switch (bc_get_ent(buf)) {
    case bc_ent.str: return bc_get_str(buf);
    case bc_ent.int: return bc_get_int(buf);
    case bc_ent.lst: return bc_get_lst(buf);
    case bc_ent.dct: return bc_get_dct(buf);
    case bc_ent.bad: break;
  }
  return null;
}

/**
 * Put unsigned integer and terminating character
 *
 * @param buf The target buffer
 * @param chr The terminating character
 * @param val The value to encode
 * @return The buffer from position immediately after terminating character or `null` when the end of buffer reached
 *
 * This function tries to encode unsigned integer to `buf`.
 *
 * This is a helper function.
 */
export function bc_put_uint_to(buf: Buffer, chr: number, val: number): Buffer | null {
  let d = 1;
  let c = 10;

  /* estimating the number of decimal digits */
  for (; val >= c; c *= 10, d++);

  if (buf.length < d + 1) {
    /* not enough space in target buffer */
    return null;
  }

  for (c = d - 1; c >= 0; c--) {
    buf.writeUInt8(0x30/*0*/ + (val % 10), c);
    val /= 10;
  }

  buf.writeUInt8(chr, d);

  return buf.slice(d + 1);
}
/**
 * Put the string entity
 *
 * @param buf The target buffer
 * @param str The string to insert
 * @return The buffer from position immediately after inserted string entity or `null` when the end of buffer reached
 */
export function bc_put_str(buf: Buffer, str: Buffer): Buffer | null {
  const res = bc_put_uint_to(buf, 0x3a/*:*/, str.length);

  if (res == null) {
    return null;
  }

  if (res.length < str.length) {
    return null;
  }

  str.copy(res);

  return res.slice(str.length);
}

/**
 * Put the string entity
 *
 * @param ptr The pointer to beginning of target buffer
 * @param val The integer value to insert
 * @return The buffer from position immediately after inserted integer entity or `null` when the end of buffer reached
 */
export function bc_put_int(buf: Buffer, val: number): Buffer | null {
  if (buf.length < 3) {
    /* not enough space in target buffer */
    return null;
  }

  buf.writeUInt8(0x69/*i*/, 0);

  if (buf.length > 3 && val < 0) {
    buf.writeUInt8(0x2d/*-*/, 1);
    buf = buf.slice(2);
    val = - val;
  } else {
    buf = buf.slice(1);
  }

  return bc_put_uint_to(buf, 0x65/*e*/, val);
}

type bc_seq_put<env_t> =
  /**
   * The callback function to put the sequence element entity
   *
   * @param buf The target buffer
   * @param env The environment of the callback function
   * @return The buffer from position immediately after inserted sequence element entity or `buf` when no elements to insert or `null` when the end of buffer reached
   *
   * This callback function must try to encode sequence element entity from beginning of buffer.
   * If no elements to insert then `buf` must be returned.
   */
  (buf: Buffer, env?: env_t) => Buffer | null;

/**
 * Put the sequence entity
 *
 * @param buf The target buffer
 * @param chr The initiating character for sequence
 * @param put The callback function which will be called to put each element of sequence
 * @param env The environment for the callback function
 * @return The buffer from position immediately after inserted sequence entity or `null` when the end of buffer reached
 */
function bc_put_seq<env_t>(buf: Buffer, chr: number, put: bc_seq_put<env_t>, env?: env_t): Buffer | null {
  if (buf.length < 2) {
    /* not enough space in target buffer */
    return null;
  }

  buf.writeUInt8(chr, 0);
  buf = buf.slice(1);

  for (; ;) {
    const res = put(buf, env);

    if (res == null) {
      return null;
    } else if (res == buf) {
      break;
    }

    buf = res;
  }

  if (buf.length < 1) {
    /* not enough space in target buffer */
    return null;
  }

  buf.writeUInt8(0x65/*e*/, 0);
  return buf.slice(1);
}

export type bc_lst_put<env_t> =
  /**
   * The callback function to put the list element entity
   *
   * @param buf The pointer to beginning of target buffer
   * @param env The environment of the callback function
   * @return The buffer from position immediately after inserted list element entity or `ptr` when no elements to insert or `null` when the end of buffer reached
   *
   * This callback function must try to encode list element entity from beginning of `buf`.
   * If no elements to insert then `buf` must be returned.
   */
  (buf: Buffer, env?: env_t) => Buffer | null;

/**
 * Put the list entity
 *
 * @param buf The target buffer
 * @param put The callback function which will be called to put each element of the list
 * @param env The environment for the callback function
 * @return The buffer from position immediately after inserted list entity or `null` when the end of buffer reached
 */
export function bc_put_lst<env_t>(buf: Buffer, put: bc_lst_put<env_t>, env?: env_t): Buffer | null {
  return bc_put_seq(buf, 0x6c/*l*/, put, env);
}

export type bc_dct_put<env_t> =
  /**
   * The callback function to put the dictionary element entity
   *
   * @param buf The target buffer
   * @param env The environment of the callback function
   * @return The buffer from position immediately after inserted dictionary element entity or `ptr` when no elements to insert or `null` when the end of buffer reached
   *
   * This callback function must try to encode dictionary element entity from beginning of buffer.
   * If no elements to insert then `ptr` must be returned.
   */
  (buf: Buffer, env?: env_t) => Buffer | null;

/**
 * Put the dictionary entity
 *
 * @param buf The target buffer
 * @param put The callback function which will be called to put each element of the dictionary
 * @param env The environment for the callback function
 * @return The buffer from position immediately after inserted dictionary entity or `null` when the end of buffer reached
 */
export function bc_put_dct<env_t>(buf: Buffer, put: bc_dct_put<env_t>, env?: env_t): Buffer | null {
  return bc_put_seq(buf, 0x64/*d*/, put, env);
}

export function bc_dct_put_str(key: Buffer, buf: Buffer, val: Buffer): Buffer | null {
  const res = bc_put_str(buf, key);

  if (res == null) {
    return null;
  }

  return bc_put_str(res, val);
}

export function bc_dct_put_int(key: Buffer, buf: Buffer, val: number): Buffer | null {
  const res = bc_put_str(buf, key);

  if (res == null) {
    return null;
  }

  return bc_put_int(res, val);
}

export function bc_dct_put_lst<env_t>(key: Buffer, buf: Buffer, put: bc_lst_put<env_t>, env?: env_t): Buffer | null {
  const res = bc_put_str(buf, key);

  if (res == null) {
    return null;
  }

  return bc_put_seq(res, 0x6c/*l*/, put, env);
}

export function bc_dct_put_dct<env_t>(key: Buffer, buf: Buffer, put: bc_dct_put<env_t>, env?: env_t): Buffer | null {
  const res = bc_put_str(buf, key);

  if (res == null) {
    return null;
  }

  return bc_put_seq(res, 0x64/*d*/, put, env);
}
