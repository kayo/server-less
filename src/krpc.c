#include <string.h>

#include "krpc.h"

/*
 * Decoder (parser) functions
 */

typedef struct {
  krpc_data_t *msg;
  bc_dct_get_f *get;
  unsigned nth;
} get_state_t;

static const char *
get_err_cb(const char *ptr,
           const char *end,
           void *env) {
  get_state_t *s = env;
  
  switch (s->nth ++) {
  case 0: return bc_get(int, ptr, end, &s->msg->code);
  case 1: return bc_get(krpc_str, ptr, end, &s->msg->msg);
  }
  
  return NULL;
}

static const char *
get_msg_cb(const char *key,
           const char *ptr,
           const char *end,
           void *env) {
  get_state_t *s = env;
  
  if (ptr - key == 1) {
    switch (*key) {
    case 'a':
    case 'r':
      return bc_get(dct, ptr, end, s->get, s->msg->env);
    case 'e':
      s->nth = 0;
      return bc_get(lst, ptr, end, get_err_cb, s);
    case 't':
      return bc_get(krpc_str, ptr, end, &s->msg->tid);
    case 'q':
      return bc_get(krpc_str, ptr, end, &s->msg->query);
    case 'v':
      return bc_get(str, ptr, end, NULL);
    case 'y': {
      const char *str;
      
      ptr = bc_get(str, ptr, end, &str);

      if (ptr != NULL &&
          ptr - str == 1) {
        switch (*str) {
        case krpc_msg_query:
        case krpc_msg_response:
        case krpc_msg_error:
          s->msg->type = *str;
          return ptr;
        }
      }
    }
    }
  }
  
  return NULL;
}

const char *
krpc_get(const char *ptr,
         const char *end,
         krpc_data_t *msg,
         bc_dct_get_f *get) {
  get_state_t s = { msg, get, 0 };
  
  return bc_get(dct, ptr, end, get_msg_cb, &s);
}

/*
 * Encoder (formatter) functions
 */

typedef struct {
  const krpc_data_t *msg;
  bc_dct_put_f *put;
  unsigned elt;
  unsigned nth;
} put_state_t;

static char *
put_query_cb(char *ptr,
             char *end,
             void *env) {
  put_state_t *s = env;
  
  switch (s->elt ++) {
  case 0:
    return bc_dct_put("a", dct, ptr, end, s->put, s->msg->env);
  case 1:
    return bc_dct_put("q", krpc_str, ptr, end, &s->msg->query);
  case 2:
    return bc_dct_put("t", krpc_str, ptr, end, &s->msg->tid);
  case 3:
    return bc_dct_put("y", cstr, ptr, end, "q");
  }
  
  return ptr;
}

static char *
put_response_cb(char *ptr,
                char *end,
                void *env) {
  put_state_t *s = env;
  
  switch (s->elt ++) {
  case 0:
    return bc_dct_put("r", dct, ptr, end, s->put, s->msg->env);
  case 1:
    return bc_dct_put("t", krpc_str, ptr, end, &s->msg->tid);
  case 2:
    return bc_dct_put("y", cstr, ptr, end, "r");
  }
  
  return ptr;
}

static char *
put_code_msg_cb(char *ptr,
                char *end,
                void *env) {
  put_state_t *s = env;
  
  switch (s->nth ++) {
  case 0:
    return bc_put(int, ptr, end, s->msg->code);
  case 1:
    return bc_put(krpc_str, ptr, end, &s->msg->msg);
  }
  
  return ptr;
}

static char *
put_error_cb(char *ptr,
                char *end,
                void *env) {
  put_state_t *s = env;
  
  switch (s->elt ++) {
  case 0:
    s->nth = 0;
    return bc_dct_put("e", lst, ptr, end, put_code_msg_cb, env);
  case 1:
    return bc_dct_put("t", krpc_str, ptr, end, &s->msg->tid);
  case 2:
    return bc_dct_put("y", cstr, ptr, end, "e");
  }
  
  return ptr;
}

char *
krpc_put(char *ptr,
         char *end,
         const krpc_data_t *msg,
         bc_dct_put_f *arg) {
  bc_dct_put_f *put;
  
  switch (msg->type) {
  case krpc_msg_query:
    put = put_query_cb;
    break;
  case krpc_msg_response:
    put = put_response_cb;
    break;
  case krpc_msg_error:
    put = put_error_cb;
    break;
  default:
    return NULL;
  }
  
  put_state_t s = { msg, arg, 0, 0 };
  
  return bc_put(dct, ptr, end, put, &s);
}
