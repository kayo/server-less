/**
 * KRPC protocol
 *
 * The embedded implementation of KRPC protocol
 *
 * The KRPC was introduced in the DHT Protocol Specification http://bittorrent.org/beps/bep_0005.html
 * This is a light and fast implementation of the KRPC protocol handler.
 */

import { bc_ref, bc_ref_get, bc_get_int, bc_get_str, bc_get_lst, bc_get_dct, bc_dct_get, bc_dct_put, bc_put_int, bc_put_str, bc_dct_put_dct, bc_dct_put_str, bc_dct_put_lst, bc_put_dct } from "./bcoder";

const key_a = Buffer.from("a");
const key_e = Buffer.from("e");
const key_r = Buffer.from("r");
const key_t = Buffer.from("t");
const key_q = Buffer.from("q");
const key_y = Buffer.from("y");

/**
 * The KRPC message type
 */
export const enum krpc_msg {
  /**
   * Query message
   */
  query = 0x71/*q*/,
  /**
   * Response message
   */
  response = 0x72/*r*/,
  /**
   * Error message
   */
  error = 0x65/*e*/,
};

/**
 * The KRPC error type
 */
export const enum krpc_err {
  /**
   * Generic Error
   */
  generic = 201,
  /**
   * @brief Server Error
   */
  server = 202,
  /**
   * @brief Protocol Error
   *
   * @i malformed packet
   * @i invalid arguments
   * @i bad token
   */
  proto = 203,
  /**
   * @brief Method unknown
   */
  method = 204,
};

/**
 * The KRPC data type
 */
export interface krpc_data<env_t> {
  /**
   * The message type
   */
  type: krpc_msg;
  /**
   * The transaction ID
   */
  tid?: Buffer;

  /**
   * The query string
   */
  query?: Buffer;
  /**
   * The environment for arguments callback
   */
  env?: env_t;

  /**
   * The error message
   */
  msg?: string;
  /**
   * The error code
   */
  code?: number;
};

/**
 * Instantiate KRPC data object for decoding
 *
 * @param env The environment for callback
 * @return The initialized data object in stack
 */
export function krpc_data_none<env_t>(env: env_t): krpc_data<env_t> {
  return {
    type: krpc_msg.error,
    env
  };
};

/**
 * Instantiate KRPC data object for encoding
 *
 * @param type The type of message
 * @param tid The transaction ID
 * @param env The environment for callback
 * @return The initialized data object in stack
 */
export function krpc_data_message<env_t>(type: krpc_msg, tid: Buffer, env: env_t): krpc_data<env_t> {
  return {
    type,
    tid,
    env
  };
};

/**
 * Instantiate KRPC data object for encoding query
 *
 * @param tid The transaction ID
 * @param query The constant string of query type
 * @param env The pointer to environment for callback
 * @return The initialized data object in stack
 */
export function krpc_data_query<env_t>(tid: Buffer, query: Buffer, env: env_t): krpc_data<env_t> {
  return {
    type: krpc_msg.query,
    tid,
    env,
    query,
  };
};

/**
 * Instantiate KRPC data object for encoding response
 *
 * @param name The name of variable
 * @param tid The transaction ID
 * @return The initialized data object in stack
 */
export function krpc_data_response<env_t>(tid: Buffer, env: env_t): krpc_data<env_t> {
  return {
    type: krpc_msg.response,
    tid,
    env
  };
};

/**
 * Instantiate KRPC data object for encoding error
 *
 * @param tid The transaction ID
 * @param code The error code
 * @param msg The error message
 * @return The initialized data object in stack
 */
export function krpc_data_error(tid: Buffer, code: number, msg: string): krpc_data<void> {
  return {
    type: krpc_msg.error,
    tid,
    code,
    msg,
  };
};

interface get_state<env_t> {
  msg: krpc_data<env_t>;
  get?: bc_dct_get<env_t>;
  nth: number;
};

function get_err_cb<env_t>(buf: Buffer, s: get_state<env_t>): Buffer | null {
  switch (s.nth++) {
    case 0: return bc_get_int(buf, bc_ref(s.msg, "code"));
    case 1: return bc_get_str(buf, bc_ref(s.msg, "msg"));
  }
  return null;
}

function get_msg_cb<env_t>(key_: Buffer, buf: Buffer, s: get_state<env_t>): Buffer | null {
  const key = key_.toString();
  switch (key) {
    case 'a':
    case 'r':
      return bc_get_dct(buf, s.get, s.msg.env);
    case 'e':
      s.nth = 0;
      return bc_get_lst(buf, get_err_cb, s);
    case 't':
      return bc_get_str(buf, bc_ref(s.msg, "tid"));
    case 'q':
      return bc_get_str(buf, bc_ref(s.msg, "query"));
    case 'v':
      return bc_get_str(buf);
    case 'y': {
      const str = bc_ref<Buffer>();
      const res = bc_get_str(buf, str);
      const key = bc_ref_get<Buffer>(str);

      if (res != null && key.length == 1) {
        const msg = key.readUInt8(0);
        switch (msg) {
          case krpc_msg.query:
          case krpc_msg.response:
          case krpc_msg.error:
            s.msg.type = msg;
            return res;
        }
      }
    }
  }

  return null;
}

/**
 * Decode KRPC message
 *
 * @param buf The source buffer
 * @param msg The message data object
 * @param get The callback function for decoding arguments or results
 * @return The buffer from position immediately after decoded entity or `null` when decoding error occurred
 */
export function krpc_get<env_t>(buf: Buffer, msg: krpc_data<env_t>, get?: bc_dct_get<env_t>): Buffer | null {
  const s: get_state<env_t> = { msg, get, nth: 0 };

  return bc_get_dct(buf, get_msg_cb, s);
}

interface put_state<env_t> {
  msg: krpc_data<env_t>;
  put?: bc_dct_put<env_t>;
  elt: number;
  nth: number;
};

function put_query_cb<env_t>(buf: Buffer, s: put_state<env_t>): Buffer | null {
  switch (s.elt++) {
    case 0:
      return bc_dct_put_dct(key_a, buf, s.put as bc_dct_put<env_t>, s.msg.env);
    case 1:
      return bc_dct_put_str(key_q, buf, Buffer.from(s.msg.query as Buffer));
    case 2:
      return bc_dct_put_str(key_t, buf, s.msg.tid as Buffer);
    case 3:
      return bc_dct_put_str(key_y, buf, key_q);
  }

  return buf;
}

function put_response_cb<env_t>(buf: Buffer, s: put_state<env_t>): Buffer | null {
  switch (s.elt++) {
    case 0:
      return bc_dct_put_dct(key_r, buf, s.put as bc_dct_put<env_t>, s.msg.env);
    case 1:
      return bc_dct_put_str(key_t, buf, s.msg.tid as Buffer);
    case 2:
      return bc_dct_put_str(key_y, buf, key_r);
  }

  return buf;
}

function put_code_msg_cb<env_t>(buf: Buffer, s: put_state<env_t>): Buffer | null {
  switch (s.nth++) {
    case 0:
      return bc_put_int(buf, s.msg.code as number);
    case 1:
      return bc_put_str(buf, Buffer.from(s.msg.msg as string));
  }

  return buf;
}

function put_error_cb<env_t>(buf: Buffer, s: put_state<env_t>): Buffer | null {
  switch (s.elt++) {
    case 0:
      s.nth = 0;
      return bc_dct_put_lst(key_e, buf, put_code_msg_cb, s);
    case 1:
      return bc_dct_put_str(key_t, buf, s.msg.tid as Buffer);
    case 2:
      return bc_dct_put_str(key_y, buf, key_e);
  }

  return buf;
}

/**
 * Encode KRPC query
 *
 * @param buf The target buffer
 * @param msg The message data object
 * @param put The callback function for encoding arguments or results
 * @return The buffer from position immediately after inserted query entity or `null` when the end of buffer reached
 */
export function krpc_put<env_t>(buf: Buffer, msg: krpc_data<env_t>, put?: bc_dct_put<env_t>): Buffer | null {
  var putf: bc_dct_put<put_state<env_t>>;

  switch (msg.type) {
    case krpc_msg.query:
      putf = put_query_cb;
      break;
    case krpc_msg.response:
      putf = put_response_cb;
      break;
    case krpc_msg.error:
      putf = put_error_cb;
      break;
    default:
      return null;
  }

  const s: put_state<env_t> = { msg, put, elt: 0, nth: 0 };

  return bc_put_dct(buf, putf, s);
}
