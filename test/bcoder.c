#include <string.h>

#include "bcoder.h"
#include "test.h"

#define outof(ptr) ((ptr) + (sizeof(ptr)))
#define endof(ptr) ((ptr) + (sizeof(ptr) - 1))
#define match(ptr, str) (0 == memcmp(ptr, str, sizeof(str) - 1))

test_main() {
  test_init();

  /*
   * Decoder (parser) tests
   */
  
  static const char dht_err[] = "d1:eli201e23:A Generic Error Ocurrede1:t2:aa1:y1:ee";

  {
    unsigned i;
    
    assert(dht_err + 3 == bc_get_uint_to(dht_err + 1, endof(dht_err), ':', &i));
    assert(1 == i);
    
    assert(dht_err + 10 == bc_get_uint_to(dht_err + 6, endof(dht_err), 'e', &i));
    assert(201 == i);

    assert(dht_err + 13 == bc_get_uint_to(dht_err + 10, endof(dht_err), ':', &i));
    assert(23 == i);
    
    assert(dht_err + 13 == bc_get_uint_to(dht_err + 11, endof(dht_err), ':', &i));
    assert(3 == i);

    assert(dht_err + 10 == bc_get_uint_to(dht_err + 6, endof(dht_err), 'e', NULL));
    
    assert(NULL == bc_get_uint_to(dht_err, endof(dht_err), ':', NULL));
    assert(NULL == bc_get_uint_to(dht_err, endof(dht_err), ':', &i));
    assert(NULL == bc_get_uint_to(dht_err + 2, endof(dht_err), ':', &i));
    assert(NULL == bc_get_uint_to(dht_err + 7, endof(dht_err), 'e', &i));
  }

  {
    assert(bc_ent_str == bc_get_ent(dht_err + 1, endof(dht_err)));
    assert(bc_ent_int == bc_get_ent(dht_err + 5, endof(dht_err)));
    assert(bc_ent_lst == bc_get_ent(dht_err + 4, endof(dht_err)));
    assert(bc_ent_dct == bc_get_ent(dht_err, endof(dht_err)));
    
    assert(bc_ent_bad == bc_get_ent(dht_err + 2, endof(dht_err)));
    assert(bc_ent_bad == bc_get_ent(dht_err + 3, endof(dht_err)));
    assert(bc_ent_bad == bc_get_ent(dht_err + 6, endof(dht_err)));
  }
  
  {
    const char *str;
    
    assert(dht_err + 4 == bc_get(str, dht_err + 1, endof(dht_err), &str));
    assert(dht_err + 3 == str);

    assert(dht_err + 36 == bc_get(str, dht_err + 10, endof(dht_err), &str));
    assert(dht_err + 13 == str);
    
    assert(dht_err + 40 == bc_get(str, dht_err + 37, endof(dht_err), &str));
    assert(dht_err + 39 == str);

    assert(dht_err + 44 == bc_get(str, dht_err + 40, endof(dht_err), &str));
    assert(dht_err + 42 == str);

    assert(dht_err + 36 == bc_get(str, dht_err + 10, endof(dht_err), NULL));
    
    assert(NULL == bc_get(str, dht_err, endof(dht_err), NULL));
    assert(NULL == bc_get(str, dht_err + 2, endof(dht_err), NULL));
  }

  {
    int val;

    assert(dht_err + 10 == bc_get(int, dht_err + 5, endof(dht_err), &val));
    assert(201 == val);
    
    assert(dht_err + 10 == bc_get(int, dht_err + 5, endof(dht_err), NULL));
  }

  {
    assert(dht_err + 37 == bc_get(lst, dht_err + 4, endof(dht_err), NULL, NULL));

    int n = 0;

    const char *get_cb1(const char *ptr,
                        const char *end,
                        void *env) {
      (void)env;
      switch (n++) {
      case 0: return bc_get(int, ptr, end, NULL);
      case 1: return bc_get(str, ptr, end, NULL);
      }
      return NULL;
    }
    
    assert(dht_err + 37 == bc_get(lst, dht_err + 4, endof(dht_err), get_cb1, NULL));
    assert(2 == n);

    n = 0;

    const char *get_cb2(const char *ptr,
                        const char *end,
                        void *env) {
      int *np = (int*)env;
      switch ((*np)++) {
      case 0: return bc_get(int, ptr, end, NULL);
      case 1: return bc_get(str, ptr, end, NULL);
      }
      return NULL;
    }
    
    assert(dht_err + 37 == bc_get(lst, dht_err + 4, endof(dht_err), get_cb2, &n));
    assert(2 == n);
    
    n = 0;
    
    const char *get_cb_buggy1(const char *ptr,
                              const char *end,
                              void *env) {
      (void)env;
      switch (++n) {
      case 0: return bc_get(int, ptr, end, NULL);
      case 1: return bc_get(str, ptr, end, NULL);
      }
      return NULL;
    }

    assert(NULL == bc_get(lst, dht_err + 4, endof(dht_err), get_cb_buggy1, NULL));
    assert(1 == n);

    n = 0;
    
    const char *get_cb_buggy2(const char *ptr,
                              const char *end,
                              void *env) {
      (void)env;
      switch (n++) {
      case 0: return bc_get(int, ptr, end, NULL);
      }
      return NULL;
    }

    assert(NULL == bc_get(lst, dht_err + 4, endof(dht_err), get_cb_buggy2, NULL));
    assert(2 == n);
  }

  {
    assert(endof(dht_err) == bc_get(dct, dht_err, endof(dht_err), NULL, NULL));

    int n = 0;

    const char *get_cb1(const char *key,
                        const char *ptr,
                        const char *end,
                        void *env) {
      (void)env;
      switch (n++) {
      case 0:
        if (ptr - key == 1 &&
            key[0] == 'e') {
          return bc_get(lst, ptr, end, NULL, NULL);
        }
        break;
      case 1:
        if (ptr - key == 1 &&
            key[0] == 't') {
          return bc_get(str, ptr, end, NULL);
        }
        break;
      case 2:
        if (ptr - key == 1 &&
            key[0] == 'y') {
          return bc_get(str, ptr, end, NULL);
        }
        break;
      }
      
      return NULL;
    }
    
    assert(endof(dht_err) == bc_get(dct, dht_err, endof(dht_err), get_cb1, NULL));
    assert(3 == n);

    n = 0;
    
    const char *get_cb2(const char *key,
                        const char *ptr,
                        const char *end,
                        void *env) {
      int *np = (int*)env;
      switch ((*np)++) {
      case 0:
        if (ptr - key == 1 &&
            key[0] == 'e') {
          return bc_get(lst, ptr, end, NULL, NULL);
        }
        break;
      case 1:
        if (ptr - key == 1 &&
            key[0] == 't') {
          return bc_get(str, ptr, end, NULL);
        }
        break;
      case 2:
        if (ptr - key == 1 &&
            key[0] == 'y') {
          return bc_get(str, ptr, end, NULL);
        }
        break;
      }
      
      return NULL;
    }
    
    assert(endof(dht_err) == bc_get(dct, dht_err, endof(dht_err), get_cb2, &n));
    assert(3 == n);

    n = 0;

    const char *get_cb_buggy1(const char *key,
                              const char *ptr,
                              const char *end,
                              void *env) {
      (void)env;
      switch (n++) {
      case 0:
        if (ptr - key == 1 &&
            key[0] == 'e') {
          return bc_get(lst, ptr, end, NULL, NULL);
        }
        break;
      case 1:
        if (ptr - key == 1 &&
            key[0] == 't') {
          return bc_get(str, ptr, end, NULL);
        }
        break;
      }
      
      return NULL;
    }
    
    assert(NULL == bc_get(dct, dht_err, endof(dht_err), get_cb_buggy1, NULL));
    assert(3 == n);

    n = 0;

    const char *get_cb_buggy2(const char *key,
                              const char *ptr,
                              const char *end,
                              void *env) {
      (void)env;
      switch (n++) {
      case 0:
        if (ptr - key == 1 &&
            key[0] == 'e') {
          return bc_get(lst, ptr, end, NULL, NULL);
        }
        break;
      }
      
      return NULL;
    }
    
    assert(NULL == bc_get(dct, dht_err, endof(dht_err), get_cb_buggy2, NULL));
    assert(2 == n);
  }

  {
    assert(dht_err + 4 == bc_get(any, dht_err + 1, endof(dht_err)));
    assert(dht_err + 37 == bc_get(any, dht_err + 4, endof(dht_err)));
    assert(dht_err + 44 == bc_get(any, dht_err + 40, endof(dht_err)));
    assert(endof(dht_err) == bc_get(any, dht_err, endof(dht_err)));

    assert(NULL == bc_get(any, dht_err + 2, endof(dht_err)));
    assert(NULL == bc_get(any, endof(dht_err) - 2, endof(dht_err)));
  }

  /*
   * Encoder (formatter) tests
   */

  {
    char buf[16];
    
    assert(buf + 2 == bc_put_uint_to(buf, outof(buf), ':', 0));
    assert(match(buf, "0:"));

    assert(buf + 2 == bc_put_uint_to(buf, outof(buf), ':', 1));
    assert(match(buf, "1:"));

    assert(buf + 3 == bc_put_uint_to(buf, outof(buf), 'e', 12));
    assert(match(buf, "12e"));

    assert(buf + 4 == bc_put_uint_to(buf, outof(buf), 'e', 123));
    assert(match(buf, "123e"));

    assert(buf + 5 == bc_put_uint_to(buf, outof(buf), 'e', 1234));
    assert(match(buf, "1234e"));

    assert(buf + 6 == bc_put_uint_to(buf, outof(buf), 'e', 12345));
    assert(match(buf, "12345e"));

    assert(buf + 7 == bc_put_uint_to(buf, outof(buf), 'e', 123456));
    assert(match(buf, "123456e"));

    assert(buf + 8 == bc_put_uint_to(buf, outof(buf), 'e', 1234567));
    assert(match(buf, "1234567e"));

    assert(buf + 9 == bc_put_uint_to(buf, outof(buf), 'e', 12345678));
    assert(match(buf, "12345678e"));

    assert(buf + 10 == bc_put_uint_to(buf, outof(buf), 'e', 123456789));
    assert(match(buf, "123456789e"));

    assert(buf + 11 == bc_put_uint_to(buf, outof(buf), 'e', 1234567890));
    assert(match(buf, "1234567890e"));

    assert(buf + 11 == bc_put_uint_to(buf, buf + 11, ':', 1234567890));
    assert(match(buf, "1234567890:"));

    assert(NULL == bc_put_uint_to(buf, buf + 10, ':', 1234567890));
  }

  {
    char buf[32];

    assert(buf + 2 == bc_put(cstr, buf, outof(buf), ""));
    assert(match(buf, "0:"));

    assert(buf + 3 == bc_put(cstr, buf, outof(buf), "e"));
    assert(match(buf, "1:e"));

    assert(buf + 4 == bc_put(cstr, buf, outof(buf), "aa"));
    assert(match(buf, "2:aa"));
    
    assert(buf + 26 == bc_put(cstr, buf, outof(buf), "A Generic Error Ocurred"));
    assert(match(buf, "23:A Generic Error Ocurred"));

    assert(buf + 26 == bc_put(cstr, buf, buf + 26, "A Generic Error Ocurred"));
    assert(match(buf, "23:A Generic Error Ocurred"));
    
    assert(NULL == bc_put(cstr, buf, buf + 25, "A Generic Error Ocurred"));
  }

  {
    char buf[8];

    assert(buf + 3 == bc_put(int, buf, outof(buf), 0));
    assert(match(buf, "i0e"));

    assert(buf + 4 == bc_put(int, buf, outof(buf), -1));
    assert(match(buf, "i-1e"));
    
    assert(buf + 5 == bc_put(int, buf, outof(buf), 201));
    assert(match(buf, "i201e"));

    assert(buf + 6 == bc_put(int, buf, outof(buf), -128));
    assert(match(buf, "i-128e"));
    
    assert(buf + 8 == bc_put(int, buf, outof(buf), 123456));
    assert(match(buf, "i123456e"));
    
    assert(NULL == bc_put(int, buf, outof(buf), 1234567));

    assert(buf + 8 == bc_put(int, buf, outof(buf), -12345));
    assert(match(buf, "i-12345e"));
    
    assert(NULL == bc_put(int, buf, outof(buf), -123456));
  }

  {
    char buf[32];
    
    int n = 0;

    char *put_cb1(char *ptr,
                  char *end,
                  void *env) {
      (void)env;
      switch (n ++) {
      case 0: return bc_put(cstr, ptr, end, "t");
      case 1: return bc_put(int, ptr, end, 202);
      case 2: return bc_put(cstr, ptr, end, "aa");
      }
      return ptr;
    }
    
    assert(buf + 14 == bc_put(lst, buf, outof(buf), put_cb1, NULL));
    assert(match(buf, "l1:ti202e2:aae"));

    n = 0;
    
    char *put_cb2(char *ptr,
                  char *end,
                  void *env) {
      int *np = (int*)env;
      switch ((*np) ++) {
      case 0: return bc_put(cstr, ptr, end, "t");
      case 1: return bc_put(int, ptr, end, 202);
      case 2: return bc_put(cstr, ptr, end, "aa");
      }
      return ptr;
    }
    
    assert(buf + 14 == bc_put(lst, buf, outof(buf), put_cb2, &n));
    assert(match(buf, "l1:ti202e2:aae"));

    n = 0;
    
    char *put_cb_buggy1(char *ptr,
                        char *end,
                        void *env) {
      int *np = (int*)env;
      switch ((*np) ++) {
      case 0: return bc_put(cstr, ptr, end, "t");
      case 1: return bc_put(int, ptr, end, 202);
      case 2: return NULL;
      }
      return ptr;
    }
    
    assert(NULL == bc_put(lst, buf, outof(buf), put_cb_buggy1, &n));
    assert(match(buf, "l1:ti202e"));

    n = 0;
    
    assert(NULL == bc_put(lst, buf, buf + 4, put_cb_buggy1, &n));
    assert(match(buf, "l1:t"));
  }

  {
    char buf[32];
    
    int n = 0;

    char *put_cb1(char *ptr,
                  char *end,
                  void *env) {
      (void)env;
      switch (n ++) {
      case 0: return bc_dct_put("x", cstr, ptr, end, "t");
      case 1: return bc_dct_put("y", int, ptr, end, 202);
      case 2: return bc_dct_put("z", cstr, ptr, end, "aa");
      }
      return ptr;
    }
    
    assert(buf + 23 == bc_put(dct, buf, outof(buf), put_cb1, NULL));
    assert(match(buf, "d1:x1:t1:yi202e1:z2:aae"));

    n = 0;
    
    char *put_cb2(char *ptr,
                  char *end,
                  void *env) {
      int *np = (int*)env;
      switch ((*np) ++) {
      case 0: return bc_dct_put("x", cstr, ptr, end, "t");
      case 1: return bc_dct_put("y", int, ptr, end, 202);
      case 2: return bc_dct_put("z", cstr, ptr, end, "aa");
      }
      return ptr;
    }
    
    assert(buf + 23 == bc_put(dct, buf, outof(buf), put_cb2, &n));
    assert(match(buf, "d1:x1:t1:yi202e1:z2:aae"));

    n = 0;
    
    char *put_cb_buggy1(char *ptr,
                        char *end,
                        void *env) {
      int *np = (int*)env;
      switch ((*np) ++) {
      case 0: return bc_dct_put("x", cstr, ptr, end, "t");
      case 1: return bc_dct_put("y", int, ptr, end, 202);
      case 2: return NULL;
      }
      return ptr;
    }
    
    assert(NULL == bc_put(dct, buf, outof(buf), put_cb_buggy1, &n));
    assert(match(buf, "d1:x1:t1:yi202e"));
    
    n = 0;
    
    assert(NULL == bc_put(dct, buf, buf + 10, put_cb_buggy1, &n));
    assert(match(buf, "d1:x1:t1:y"));
  }
  
  test_done();
}
