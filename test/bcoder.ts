import "mocha";
import * as assert from "assert";
import { bc_ref, bc_ref_get, bc_ent, bc_get_ent, bc_get_uint_to, bc_get_str, bc_get_int, bc_get_lst, bc_get_dct, bc_get_any, bc_put_uint_to, bc_put_str, bc_put_int, bc_put_lst, bc_put_dct, bc_dct_put_str, bc_dct_put_int } from "../src/bcoder";

describe("Test bcoder", () => {
  describe("Decoder (parser) tests", () => {
    const dht_err = Buffer.from("d1:eli201e23:A Generic Error Ocurrede1:t2:aa1:y1:ee");

    it("bc_get_uint_to()", () => {
      const val = bc_ref<number>();

      assert.deepEqual(dht_err.slice(3), bc_get_uint_to(dht_err.slice(1), 0x3a/*:*/, val));
      assert.equal(1, bc_ref_get(val));

      assert.deepEqual(dht_err.slice(10), bc_get_uint_to(dht_err.slice(6), 0x65/*e*/, val));
      assert.equal(201, bc_ref_get(val));

      assert.deepEqual(dht_err.slice(13), bc_get_uint_to(dht_err.slice(10), 0x3a/*:*/, val));
      assert.equal(23, bc_ref_get(val));

      assert.deepEqual(dht_err.slice(13), bc_get_uint_to(dht_err.slice(11), 0x3a/*:*/, val));
      assert.equal(3, bc_ref_get(val));

      assert.deepEqual(dht_err.slice(10), bc_get_uint_to(dht_err.slice(6), 0x65/*e*/));

      assert.deepEqual(null, bc_get_uint_to(dht_err, 0x3a/*:*/));
      assert.deepEqual(null, bc_get_uint_to(dht_err, 0x3a/*:*/));
      assert.deepEqual(null, bc_get_uint_to(dht_err.slice(2), 0x3a/*:*/));
      assert.deepEqual(null, bc_get_uint_to(dht_err.slice(7), 0x65/*e*/));
    });

    it("bc_get_ent()", () => {
      assert(bc_ent.str == bc_get_ent(dht_err.slice(1)));
      assert(bc_ent.int == bc_get_ent(dht_err.slice(5)));
      assert(bc_ent.lst == bc_get_ent(dht_err.slice(4)));
      assert(bc_ent.dct == bc_get_ent(dht_err));

      assert(bc_ent.bad == bc_get_ent(dht_err.slice(2)));
      assert(bc_ent.bad == bc_get_ent(dht_err.slice(3)));
      assert(bc_ent.bad == bc_get_ent(dht_err.slice(6)));
    });

    it("bc_get_str()", () => {
      const val = bc_ref<string>();

      assert.deepEqual(dht_err.slice(4), bc_get_str(dht_err.slice(1), val));
      assert.equal(dht_err.toString('ascii', 3, 3 + bc_ref_get<string>(val).length), bc_ref_get(val));

      assert.deepEqual(dht_err.slice(36), bc_get_str(dht_err.slice(10), val));
      assert.equal(dht_err.toString('ascii', 13, 13 + bc_ref_get<string>(val).length), bc_ref_get(val));

      assert.deepEqual(dht_err.slice(40), bc_get_str(dht_err.slice(37), val));
      assert.equal(dht_err.toString('ascii', 39, 39 + bc_ref_get<string>(val).length), bc_ref_get(val));

      assert.deepEqual(dht_err.slice(44), bc_get_str(dht_err.slice(40), val));
      assert.equal(dht_err.toString('ascii', 42, 42 + bc_ref_get<string>(val).length), bc_ref_get(val));

      assert.deepEqual(dht_err.slice(36), bc_get_str(dht_err.slice(10), val));

      assert.deepEqual(null, bc_get_str(dht_err));
      assert.deepEqual(null, bc_get_str(dht_err.slice(2)));
    });

    it("bc_get_int()", () => {
      const val = bc_ref<number>();

      assert.deepEqual(dht_err.slice(10), bc_get_int(dht_err.slice(5), val));
      assert.equal(201, bc_ref_get(val));

      assert.deepEqual(dht_err.slice(10), bc_get_int(dht_err.slice(5), val));
    });

    it("bc_get_lst()", () => {
      assert.deepEqual(dht_err.slice(37), bc_get_lst(dht_err.slice(4)));

      let n = 0;

      const get_cb1 = function(buf: Buffer): Buffer | null {
        switch (n++) {
          case 0: return bc_get_int(buf);
          case 1: return bc_get_str(buf);
        }
        return null;
      };

      assert.deepEqual(dht_err.slice(37), bc_get_lst(dht_err.slice(4), get_cb1));
      assert.equal(2, n);

      const get_cb2 = function(buf: Buffer, np: number[]): Buffer | null {
        switch (np[0]++) {
          case 0: return bc_get_int(buf);
          case 1: return bc_get_str(buf);
        }
        return null;
      };

      let np = [0];
      assert.deepEqual(dht_err.slice(37), bc_get_lst(dht_err.slice(4), get_cb2, np));
      assert.equal(2, np[0]);

      n = 0;

      const get_cb_buggy1 = function(buf: Buffer): Buffer | null {
        switch (++n) {
          case 0: return bc_get_int(buf);
          case 1: return bc_get_str(buf);
        }
        return null;
      }

      assert.deepEqual(null, bc_get_lst(dht_err.slice(4), get_cb_buggy1));
      assert.equal(1, n);

      n = 0;

      const get_cb_buggy2 = function(buf: Buffer): Buffer | null {
        switch (n++) {
          case 0: return bc_get_int(buf);
        }
        return null;
      }

      assert.deepEqual(null, bc_get_lst(dht_err.slice(4), get_cb_buggy2));
      assert.equal(2, n);
    });

    it("bc_get_dct()", () => {
      assert.deepEqual(dht_err.slice(dht_err.length), bc_get_dct(dht_err));

      let n = 0;

      const get_cb1 = function(key_: Buffer, buf: Buffer): Buffer | null {
        const key = key_.toString();
        switch (n++) {
          case 0:
            if (key == 'e') {
              return bc_get_lst(buf);
            }
            break;
          case 1:
            if (key == 't') {
              return bc_get_str(buf);
            }
            break;
          case 2:
            if (key == 'y') {
              return bc_get_str(buf);
            }
            break;
        }

        return null;
      };

      assert.deepEqual(dht_err.slice(dht_err.length), bc_get_dct(dht_err, get_cb1));
      assert.equal(3, n);

      let np = [0];

      const get_cb2 = function(key_: Buffer, buf: Buffer, np: number[]): Buffer | null {
        const key = key_.toString();
        switch (np[0]++) {
          case 0:
            if (key == 'e') {
              return bc_get_lst(buf);
            }
            break;
          case 1:
            if (key == 't') {
              return bc_get_str(buf);
            }
            break;
          case 2:
            if (key == 'y') {
              return bc_get_str(buf);
            }
            break;
        }

        return null;
      };

      assert.deepEqual(dht_err.slice(dht_err.length), bc_get_dct(dht_err, get_cb2, np));
      assert.equal(3, np[0]);

      n = 0;

      const get_cb_buggy1 = function(key_: Buffer, buf: Buffer): Buffer | null {
        const key = key_.toString();
        switch (n++) {
          case 0:
            if (key == 'e') {
              return bc_get_lst(buf);
            }
            break;
          case 1:
            if (key == 't') {
              return bc_get_str(buf);
            }
            break;
        }

        return null;
      };

      assert.deepEqual(null, bc_get_dct(dht_err, get_cb_buggy1));
      assert.equal(3, n);

      n = 0;

      const get_cb_buggy2 = function(key: Buffer, buf: Buffer): Buffer | null {
        switch (n++) {
          case 0:
            if (key.toString() == 'e') {
              return bc_get_lst(buf);
            }
            break;
        }

        return null;
      };

      assert.deepEqual(null, bc_get_dct(dht_err, get_cb_buggy2));
      assert.equal(2, n);
    });

    it("bc_get_any()", () => {
      assert.deepEqual(dht_err.slice(4), bc_get_any(dht_err.slice(1)));
      assert.deepEqual(dht_err.slice(37), bc_get_any(dht_err.slice(4)));
      assert.deepEqual(dht_err.slice(44), bc_get_any(dht_err.slice(40)));
      assert.deepEqual(dht_err.slice(dht_err.length), bc_get_any(dht_err));

      assert.deepEqual(null, bc_get_any(dht_err.slice(2)));
      assert.deepEqual(null, bc_get_any(dht_err.slice(dht_err.length - 2)));
    });
  });

  const match = function(buf: Buffer, str: string): void {
    assert.equal(buf.toString('ascii', 0, str.length), str);
  };

  describe("Encoder (formatter) tests", () => {
    it("bc_put_uint_to()", () => {
      const buf = Buffer.alloc(16);

      assert.deepEqual(buf.slice(2), bc_put_uint_to(buf, 0x3a/*:*/, 0));
      match(buf, "0:");

      assert.deepEqual(buf.slice(2), bc_put_uint_to(buf, 0x3a/*:*/, 1));
      match(buf, "1:");

      assert.deepEqual(buf.slice(3), bc_put_uint_to(buf, 0x65/*e*/, 12));
      match(buf, "12e");

      assert.deepEqual(buf.slice(4), bc_put_uint_to(buf, 0x65/*e*/, 123));
      match(buf, "123e");

      assert.deepEqual(buf.slice(5), bc_put_uint_to(buf, 0x65/*e*/, 1234));
      match(buf, "1234e");

      assert.deepEqual(buf.slice(6), bc_put_uint_to(buf, 0x65/*e*/, 12345));
      match(buf, "12345e");

      assert.deepEqual(buf.slice(7), bc_put_uint_to(buf, 0x65/*e*/, 123456));
      match(buf, "123456e");

      assert.deepEqual(buf.slice(8), bc_put_uint_to(buf, 0x65/*e*/, 1234567));
      match(buf, "1234567e");

      assert.deepEqual(buf.slice(9), bc_put_uint_to(buf, 0x65/*e*/, 12345678));
      match(buf, "12345678e");

      assert.deepEqual(buf.slice(10), bc_put_uint_to(buf, 0x65/*e*/, 123456789));
      match(buf, "123456789e");

      assert.deepEqual(buf.slice(11), bc_put_uint_to(buf, 0x65/*e*/, 1234567890));
      match(buf, "1234567890e");

      assert.deepEqual(buf.slice(buf.length), bc_put_uint_to(buf.slice(buf.length - 11), 0x3a/*:*/, 1234567890));
      match(buf.slice(buf.length - 11), "1234567890:");

      assert.deepEqual(null, bc_put_uint_to(buf.slice(buf.length - 10), 0x3a/*:*/, 1234567890));
    });

    it("bc_put_str()", () => {
      const buf = Buffer.alloc(32);

      assert.deepEqual(buf.slice(2), bc_put_str(buf, Buffer.from("")));
      match(buf, "0:");

      assert.deepEqual(buf.slice(3), bc_put_str(buf, Buffer.from("e")));
      match(buf, "1:e");

      assert.deepEqual(buf.slice(4), bc_put_str(buf, Buffer.from("aa")));
      match(buf, "2:aa");

      assert.deepEqual(buf.slice(26), bc_put_str(buf, Buffer.from("A Generic Error Ocurred")));
      match(buf, "23:A Generic Error Ocurred");

      assert.deepEqual(buf.slice(buf.length), bc_put_str(buf.slice(buf.length - 26), Buffer.from("A Generic Error Ocurred")));
      match(buf.slice(buf.length - 26), "23:A Generic Error Ocurred");

      assert.deepEqual(null, bc_put_str(buf.slice(buf.length - 25), Buffer.from("A Generic Error Ocurred")));
    });

    it("bc_put_int()", () => {
      const buf = Buffer.alloc(8);

      assert.deepEqual(buf.slice(3), bc_put_int(buf, 0));
      match(buf, "i0e");

      assert.deepEqual(buf.slice(4), bc_put_int(buf, -1));
      match(buf, "i-1e");

      assert.deepEqual(buf.slice(5), bc_put_int(buf, 201));
      match(buf, "i201e");

      assert.deepEqual(buf.slice(6), bc_put_int(buf, -128));
      match(buf, "i-128e");

      assert.deepEqual(buf.slice(8), bc_put_int(buf, 123456));
      match(buf, "i123456e");

      assert.deepEqual(null, bc_put_int(buf, 1234567));

      assert.deepEqual(buf.slice(8), bc_put_int(buf, -12345));
      match(buf, "i-12345e");

      assert.deepEqual(null, bc_put_int(buf, -123456));
    });

    it("bc_put_lst()", () => {
      const buf = Buffer.alloc(32);

      let n = 0;

      const put_cb1 = function(buf: Buffer): Buffer | null {
        switch (n++) {
          case 0: return bc_put_str(buf, Buffer.from("t"));
          case 1: return bc_put_int(buf, 202);
          case 2: return bc_put_str(buf, Buffer.from("aa"));
        }
        return buf;
      };

      assert.deepEqual(buf.slice(14), bc_put_lst(buf, put_cb1));
      match(buf, "l1:ti202e2:aae");

      const np = [0];

      const put_cb2 = function(buf: Buffer, np: number[]): Buffer | null {
        switch (np[0]++) {
          case 0: return bc_put_str(buf, Buffer.from("t"));
          case 1: return bc_put_int(buf, 202);
          case 2: return bc_put_str(buf, Buffer.from("aa"));
        }
        return buf;
      };

      assert.deepEqual(buf.slice(14), bc_put_lst(buf, put_cb2, np));
      match(buf, "l1:ti202e2:aae");

      np[0] = 0;

      const put_cb_buggy1 = function(buf: Buffer, np: number[]): Buffer | null {
        switch (np[0]++) {
          case 0: return bc_put_str(buf, Buffer.from("t"));
          case 1: return bc_put_int(buf, 202);
          case 2: return null;
        }
        return buf;
      };

      assert.deepEqual(null, bc_put_lst(buf, put_cb_buggy1, np));
      match(buf, "l1:ti202e");

      np[0] = 0;

      assert.deepEqual(null, bc_put_lst(buf.slice(buf.length - 4), put_cb_buggy1, np));
      match(buf, "l1:t");
    });

    it("bc_put_dct()", () => {
      const buf = Buffer.alloc(32);

      let n = 0;

      const put_cb1 = function(buf: Buffer): Buffer | null {
        switch (n++) {
          case 0: return bc_dct_put_str(Buffer.from("x"), buf, Buffer.from("t"));
          case 1: return bc_dct_put_int(Buffer.from("y"), buf, 202);
          case 2: return bc_dct_put_str(Buffer.from("z"), buf, Buffer.from("aa"));
        }
        return buf;
      };

      assert.deepEqual(buf.slice(23), bc_put_dct(buf, put_cb1, null));
      match(buf, "d1:x1:t1:yi202e1:z2:aae");

      const np = [0];

      const put_cb2 = function(buf: Buffer, np: number[]): Buffer | null {
        switch (np[0]++) {
          case 0: return bc_dct_put_str(Buffer.from("x"), buf, Buffer.from("t"));
          case 1: return bc_dct_put_int(Buffer.from("y"), buf, 202);
          case 2: return bc_dct_put_str(Buffer.from("z"), buf, Buffer.from("aa"));
        }
        return buf;
      };

      assert.deepEqual(buf.slice(23), bc_put_dct(buf, put_cb2, np));
      match(buf, "d1:x1:t1:yi202e1:z2:aae");

      np[0] = 0;

      const put_cb_buggy1 = function(buf: Buffer, np: number[]) {
        switch (np[0]++) {
          case 0: return bc_dct_put_str(Buffer.from("x"), buf, Buffer.from("t"));
          case 1: return bc_dct_put_int(Buffer.from("y"), buf, 202);
          case 2: return null;
        }
        return buf;
      };

      assert.deepEqual(null, bc_put_dct(buf, put_cb_buggy1, np));
      match(buf, "d1:x1:t1:yi202e");

      np[0] = 0;

      assert.deepEqual(null, bc_put_dct(buf.slice(buf.length - 10), put_cb_buggy1, np));
      match(buf, "d1:x1:t1:y");
    });
  });
});
