#include <string.h>

#include "krpc.h"
#include "test.h"

#define endof(ptr) ((ptr) + (sizeof(ptr) - 1))
#define match(ptr, end, str)                    \
  ((end - ptr == sizeof(str) - 1) &&            \
   (0 == memcmp(ptr, str, sizeof(str) - 1)))
#define smatch(src, str)                            \
  (((src)->end - (src)->ptr == sizeof(str) - 1) &&  \
   (0 == memcmp((src)->ptr, str, sizeof(str) - 1)))

test_main() {
  test_init();

  /* Error message */

  { /* decode */
    const char buf[] = "d1:eli201e23:A Generic Error Ocurrede1:t2:aa1:y1:ee";
    
    krpc_data(none, msg, NULL);
    
    assert(endof(buf) == krpc_get(buf, endof(buf), &msg, NULL));
    assert(msg.type == krpc_msg_error);
    assert(smatch(&msg.tid, "aa"));
    assert(msg.code == 201);
    assert(smatch(&msg.msg, "A Generic Error Ocurred"));
  }

  { /* encode */
    char buf[64];
    
    krpc_data(error, msg, krpc_cstr("aa"), 201,
              krpc_cstr("A Generic Error Ocurred"));

    char *end = krpc_put(buf, endof(buf), &msg, NULL);
    assert(match(buf, end, "d1:eli201e23:A Generic Error Ocurrede1:t2:aa1:y1:ee"));
  }

  /* DHT ping */

  { /* decode query */
    const char buf[] = "d1:ad2:id20:abcdefghij0123456789e1:q4:ping1:t2:aa1:y1:qe";
    
    krpc_data(none, msg, NULL);
    
    int n = 0;
    const char *get(const char *key,
                    const char *ptr,
                    const char *end,
                    void *env) {
      (void)env;
      switch (n ++) {
      case 0:
        assert(match(key, ptr, "id"));
        ptr = bc_get(str, ptr, end, &key);
        if (!ptr) break;
        assert(match(key, ptr, "abcdefghij0123456789"));
        return ptr;
      }
      return NULL;
    }

    const char *end = krpc_get(buf, endof(buf), &msg, get);
    assert(end == endof(buf));
    assert(msg.type == krpc_msg_query);
    assert(smatch(&msg.tid, "aa"));
    assert(smatch(&msg.query, "ping"));
    assert(n == 1);
  }

  { /* encode query */
    char buf[64];
    
    krpc_data(query, msg, krpc_cstr("aa"), "ping", NULL);

    int n = 0;
    char *put(char *ptr,
              char *end,
              void *env) {
      (void)env;
      switch (n ++) {
      case 0:
        return bc_dct_put("id", cstr, ptr, end, "abcdefghij0123456789");
      case 1:
        return ptr;
      }
      return NULL;
    }
    
    char *end = krpc_put(buf, endof(buf), &msg, put);
    assert(n == 2);
    assert(match(buf, end, "d1:ad2:id20:abcdefghij0123456789e1:q4:ping1:t2:aa1:y1:qe"));
  }

  { /* decode response */
    const char buf[] = "d1:rd2:id20:mnopqrstuvwxyz123456e1:t2:aa1:y1:re";
    
    krpc_data(none, msg, NULL);

    int n = 0;
    const char *get(const char *key,
                    const char *ptr,
                    const char *end,
                    void *env) {
      (void)env;
      switch (n ++) {
      case 0:
        assert(match(key, ptr, "id"));
        ptr = bc_get(str, ptr, end, &key);
        if (!ptr) break;
        assert(match(key, ptr, "mnopqrstuvwxyz123456"));
        return ptr;
      }
      return NULL;
    }

    const char *end = krpc_get(buf, endof(buf), &msg, get);
    assert(end == endof(buf));
    assert(msg.type == krpc_msg_response);
    assert(smatch(&msg.tid, "aa"));
    assert(n == 1);
  }
  
  { /* encode response */
    char buf[64];
    
    krpc_data(response, msg, krpc_cstr("aa"), NULL);

    int n = 0;
    char *put(char *ptr,
              char *end,
              void *env) {
      (void)env;
      switch (n ++) {
      case 0:
        return bc_dct_put("id", cstr, ptr, end, "mnopqrstuvwxyz123456");
      case 1:
        return ptr;
      }
      return NULL;
    }

    char *end = krpc_put(buf, endof(buf), &msg, put);
    assert(n == 2);
    assert(match(buf, end, "d1:rd2:id20:mnopqrstuvwxyz123456e1:t2:aa1:y1:re"));
  }

  /* DHT find_node */
  
  { /* decode query */
    const char buf[] = "d1:ad2:id20:abcdefghij01234567896:target20:mnopqrstuvwxyz123456e1:q9:find_node1:t2:aa1:y1:qe";
    
    krpc_data(none, msg, NULL);

    int n = 0;
    const char *get(const char *key,
                    const char *ptr,
                    const char *end,
                    void *env) {
      (void)env;
      switch (n ++) {
      case 0:
        assert(match(key, ptr, "id"));
        ptr = bc_get(str, ptr, end, &key);
        if (!ptr) break;
        assert(match(key, ptr, "abcdefghij0123456789"));
        return ptr;
      case 1:
        assert(match(key, ptr, "target"));
        ptr = bc_get(str, ptr, end, &key);
        if (!ptr) break;
        assert(match(key, ptr, "mnopqrstuvwxyz123456"));
        return ptr;
      }
      return NULL;
    }

    const char *end = krpc_get(buf, endof(buf), &msg, get);
    assert(end == endof(buf));
    assert(msg.type == krpc_msg_query);
    assert(smatch(&msg.tid, "aa"));
    assert(smatch(&msg.query, "find_node"));
    assert(n == 2);
  }

  { /* encode query */
    char buf[128];
    
    krpc_data(query, msg, krpc_cstr("aa"), "find_node", NULL);

    int n = 0;
    char *put(char *ptr,
              char *end,
              void *env) {
      (void)env;
      switch (n ++) {
      case 0:
        return bc_dct_put("id", cstr, ptr, end, "abcdefghij0123456789");
      case 1:
        return bc_dct_put("target", cstr, ptr, end, "mnopqrstuvwxyz123456");
      case 2:
        return ptr;
      }
      return NULL;
    }

    char *end = krpc_put(buf, endof(buf), &msg, put);
    assert(n == 3);
    assert(match(buf, end, "d1:ad2:id20:abcdefghij01234567896:target20:mnopqrstuvwxyz123456e1:q9:find_node1:t2:aa1:y1:qe"));
  }

  { /* decode response */
    const char buf[] = "d1:rd2:id20:0123456789abcdefghij5:nodes9:def456...e1:t2:aa1:y1:re";
    
    krpc_data(none, msg, NULL);
    
    int n = 0;
    const char *get(const char *key,
                    const char *ptr,
                    const char *end,
                    void *env) {
      (void)env;
      switch (n ++) {
      case 0:
        assert(match(key, ptr, "id"));
        ptr = bc_get(str, ptr, end, &key);
        if (!ptr) break;
        assert(match(key, ptr, "0123456789abcdefghij"));
        return ptr;
      case 1:
        assert(match(key, ptr, "nodes"));
        ptr = bc_get(str, ptr, end, &key);
        if (!ptr) break;
        assert(match(key, ptr, "def456..."));
        return ptr;
      }
      return NULL;
    }

    const char *end = krpc_get(buf, endof(buf), &msg, get);
    assert(end == endof(buf));
    assert(msg.type == krpc_msg_response);
    assert(smatch(&msg.tid, "aa"));
    assert(n == 2);
  }
  
  { /* encode response */
    char buf[128];
    
    krpc_data(response, msg, krpc_cstr("aa"), NULL);
    
    int n = 0;
    char *put(char *ptr,
              char *end,
              void *env) {
      (void)env;
      switch (n ++) {
      case 0:
        return bc_dct_put("id", cstr, ptr, end, "0123456789abcdefghij");
      case 1:
        return bc_dct_put("nodes", cstr, ptr, end, "def456...");
      case 2:
        return ptr;
      }
      return NULL;
    }

    char *end = krpc_put(buf, endof(buf), &msg, put);
    assert(n == 3);
    assert(match(buf, end, "d1:rd2:id20:0123456789abcdefghij5:nodes9:def456...e1:t2:aa1:y1:re"));
  }

  /* DHT get_peers */
  
  { /* decode query */
    const char buf[] = "d1:ad2:id20:abcdefghij01234567899:info_hash20:mnopqrstuvwxyz123456e1:q9:get_peers1:t2:aa1:y1:qe";
    
    krpc_data(none, msg, NULL);

    int n = 0;
    const char *get(const char *key,
                    const char *ptr,
                    const char *end,
                    void *env) {
      (void)env;
      switch (n ++) {
      case 0:
        assert(match(key, ptr, "id"));
        ptr = bc_get(str, ptr, end, &key);
        if (!ptr) break;
        assert(match(key, ptr, "abcdefghij0123456789"));
        return ptr;
      case 1:
        assert(match(key, ptr, "info_hash"));
        ptr = bc_get(str, ptr, end, &key);
        if (!ptr) break;
        assert(match(key, ptr, "mnopqrstuvwxyz123456"));
        return ptr;
      }
      return NULL;
    }

    const char *end = krpc_get(buf, endof(buf), &msg, get);
    assert(end == endof(buf));
    assert(msg.type == krpc_msg_query);
    assert(smatch(&msg.tid, "aa"));
    assert(smatch(&msg.query, "get_peers"));
    assert(n == 2);
  }

  { /* encode query */
    char buf[128];
    
    krpc_data(query, msg, krpc_cstr("aa"), "get_peers", NULL);

    int n = 0;
    char *put(char *ptr,
              char *end,
              void *env) {
      (void)env;
      switch (n ++) {
      case 0:
        return bc_dct_put("id", cstr, ptr, end, "abcdefghij0123456789");
      case 1:
        return bc_dct_put("info_hash", cstr, ptr, end, "mnopqrstuvwxyz123456");
      case 2:
        return ptr;
      }
      return NULL;
    }

    char *end = krpc_put(buf, endof(buf), &msg, put);
    assert(n == 3);
    assert(match(buf, end, "d1:ad2:id20:abcdefghij01234567899:info_hash20:mnopqrstuvwxyz123456e1:q9:get_peers1:t2:aa1:y1:qe"));
  }

  { /* decode response 1 */
    const char buf[] = "d1:rd2:id20:abcdefghij01234567895:token8:aoeusnth6:valuesl6:axje.u6:idhtnmee1:t2:aa1:y1:re";
    
    krpc_data(none, msg, NULL);

    int m = 0;
    const char *get_val(const char *ptr,
                        const char *end,
                        void *env) {
      (void)env;
      const char *val;
      ptr = bc_get(str, ptr, end, &val);
      if (ptr != NULL) {
        switch (m ++) {
        case 0:
          assert(match(val, ptr, "axje.u"));
          return ptr;
        case 1:
          assert(match(val, ptr, "idhtnm"));
          return ptr;
        }
      }
      return NULL;
    }
    
    int n = 0;
    const char *get(const char *key,
                    const char *ptr,
                    const char *end,
                    void *env) {
      (void)env;
      switch (n ++) {
      case 0:
        assert(match(key, ptr, "id"));
        ptr = bc_get(str, ptr, end, &key);
        if (!ptr) break;
        assert(match(key, ptr, "abcdefghij0123456789"));
        return ptr;
      case 1:
        assert(match(key, ptr, "token"));
        ptr = bc_get(str, ptr, end, &key);
        if (!ptr) break;
        assert(match(key, ptr, "aoeusnth"));
        return ptr;
      case 2:
        assert(match(key, ptr, "values"));
        return bc_get(lst, ptr, end, get_val, NULL);
      }
      return NULL;
    }

    const char *end = krpc_get(buf, endof(buf), &msg, get);
    assert(end == endof(buf));
    assert(msg.type == krpc_msg_response);
    assert(smatch(&msg.tid, "aa"));
    assert(m == 2);
    assert(n == 3);
  }
  
  { /* encode response 1 */
    char buf[128];
    
    krpc_data(response, msg, krpc_cstr("aa"), NULL);

    int m = 0;
    char *put_val(char *ptr,
                  char *end,
                  void *env) {
      (void)env;
      switch (m ++) {
      case 0:
        return bc_put(cstr, ptr, end, "axje.u");
      case 1:
        return bc_put(cstr, ptr, end, "idhtnm");
      case 2:
        return ptr;
      }
      return NULL;
    }
    
    int n = 0;
    char *put(char *ptr,
              char *end,
              void *env) {
      (void)env;
      switch (n ++) {
      case 0:
        return bc_dct_put("id", cstr, ptr, end, "abcdefghij0123456789");
      case 1:
        return bc_dct_put("token", cstr, ptr, end, "aoeusnth");
      case 2:
        return bc_dct_put("values", lst, ptr, end, put_val, NULL);
      case 3:
        return ptr;
      }
      return NULL;
    }

    char *end = krpc_put(buf, endof(buf), &msg, put);
    assert(n == 4);
    assert(m == 3);
    assert(match(buf, end, "d1:rd2:id20:abcdefghij01234567895:token8:aoeusnth6:valuesl6:axje.u6:idhtnmee1:t2:aa1:y1:re"));
  }

  { /* decode response 2 */
    const char buf[] = "d1:rd2:id20:abcdefghij01234567895:nodes9:def456...5:token8:aoeusnthe1:t2:aa1:y1:re";
    
    krpc_data(none, msg, NULL);
    
    int n = 0;
    const char *get(const char *key,
                    const char *ptr,
                    const char *end,
                    void *env) {
      (void)env;
      switch (n ++) {
      case 0:
        assert(match(key, ptr, "id"));
        ptr = bc_get(str, ptr, end, &key);
        if (!ptr) break;
        assert(match(key, ptr, "abcdefghij0123456789"));
        return ptr;
      case 1:
        assert(match(key, ptr, "nodes"));
        ptr = bc_get(str, ptr, end, &key);
        if (!ptr) break;
        assert(match(key, ptr, "def456..."));
        return ptr;
      case 2:
        assert(match(key, ptr, "token"));
        ptr = bc_get(str, ptr, end, &key);
        if (!ptr) break;
        assert(match(key, ptr, "aoeusnth"));
        return ptr;
      }
      return NULL;
    }

    const char *end = krpc_get(buf, endof(buf), &msg, get);
    assert(end == endof(buf));
    assert(msg.type == krpc_msg_response);
    assert(smatch(&msg.tid, "aa"));
    assert(n == 3);
  }
  
  { /* encode response 2 */
    char buf[128];
    
    krpc_data(response, msg, krpc_cstr("aa"), NULL);
    
    int n = 0;
    char *put(char *ptr,
              char *end,
              void *env) {
      (void)env;
      switch (n ++) {
      case 0:
        return bc_dct_put("id", cstr, ptr, end, "abcdefghij0123456789");
      case 1:
        return bc_dct_put("nodes", cstr, ptr, end, "def456...");
      case 2:
        return bc_dct_put("token", cstr, ptr, end, "aoeusnth");
      case 3:
        return ptr;
      }
      return NULL;
    }

    char *end = krpc_put(buf, endof(buf), &msg, put);
    assert(n == 4);
    assert(match(buf, end, "d1:rd2:id20:abcdefghij01234567895:nodes9:def456...5:token8:aoeusnthe1:t2:aa1:y1:re"));
  }

  /* DHT announce_peer */
  
  { /* decode query */
    const char buf[] = "d1:ad2:id20:abcdefghij012345678912:implied_porti1e9:info_hash20:mnopqrstuvwxyz1234564:porti6881e5:token8:aoeusnthe1:q13:announce_peer1:t2:aa1:y1:qe";
    
    krpc_data(none, msg, NULL);

    int n = 0;
    const char *get(const char *key,
                    const char *ptr,
                    const char *end,
                    void *env) {
      (void)env;
      int num;
      switch (n ++) {
      case 0:
        assert(match(key, ptr, "id"));
        ptr = bc_get(str, ptr, end, &key);
        if (!ptr) break;
        assert(match(key, ptr, "abcdefghij0123456789"));
        return ptr;
      case 1:
        assert(match(key, ptr, "implied_port"));
        ptr = bc_get(int, ptr, end, &num);
        if (!ptr) break;
        assert(num == 1);
        return ptr;
      case 2:
        assert(match(key, ptr, "info_hash"));
        ptr = bc_get(str, ptr, end, &key);
        if (!ptr) break;
        assert(match(key, ptr, "mnopqrstuvwxyz123456"));
        return ptr;
      case 3:
        assert(match(key, ptr, "port"));
        ptr = bc_get(int, ptr, end, &num);
        if (!ptr) break;
        assert(num == 6881);
        return ptr;
      case 4:
        assert(match(key, ptr, "token"));
        ptr = bc_get(str, ptr, end, &key);
        if (!ptr) break;
        assert(match(key, ptr, "aoeusnth"));
        return ptr;
      }
      return NULL;
    }

    const char *end = krpc_get(buf, endof(buf), &msg, get);
    assert(end == endof(buf));
    assert(msg.type == krpc_msg_query);
    assert(smatch(&msg.tid, "aa"));
    assert(smatch(&msg.query, "announce_peer"));
    assert(n == 5);
  }

  { /* encode query */
    char buf[256];
    
    krpc_data(query, msg, krpc_cstr("aa"), "announce_peer", NULL);

    int n = 0;
    char *put(char *ptr,
              char *end,
              void *env) {
      (void)env;
      switch (n ++) {
      case 0:
        return bc_dct_put("id", cstr, ptr, end, "abcdefghij0123456789");
      case 1:
        return bc_dct_put("implied_port", int, ptr, end, 1);
      case 2:
        return bc_dct_put("info_hash", cstr, ptr, end, "mnopqrstuvwxyz123456");
      case 3:
        return bc_dct_put("port", int, ptr, end, 6881);
      case 4:
        return bc_dct_put("token", cstr, ptr, end, "aoeusnth");
      case 5:
        return ptr;
      }
      return NULL;
    }
    
    char *end = krpc_put(buf, endof(buf), &msg, put);
    assert(n == 6);
    const char res[] = "d1:ad2:id20:abcdefghij012345678912:implied_porti1e9:info_hash20:mnopqrstuvwxyz1234564:porti6881e5:token8:aoeusnthe1:q13:announce_peer1:t2:aa1:y1:qe";
    assert(match(buf, end, res));
  }
  
  { /* decode response */
    const char buf[] = "d1:rd2:id20:mnopqrstuvwxyz123456e1:t2:aa1:y1:re";
    
    krpc_data(none, msg, NULL);
    
    int n = 0;
    const char *get(const char *key,
                    const char *ptr,
                    const char *end,
                    void *env) {
      (void)env;
      switch (n ++) {
      case 0:
        assert(match(key, ptr, "id"));
        ptr = bc_get(str, ptr, end, &key);
        if (!ptr) break;
        assert(match(key, ptr, "mnopqrstuvwxyz123456"));
        return ptr;
      }
      return NULL;
    }

    const char *end = krpc_get(buf, endof(buf), &msg, get);
    assert(end == endof(buf));
    assert(msg.type == krpc_msg_response);
    assert(smatch(&msg.tid, "aa"));
    assert(n == 1);
  }
  
  { /* encode response */
    char buf[128];
    
    krpc_data(response, msg, krpc_cstr("aa"), NULL);
    
    int n = 0;
    char *put(char *ptr,
              char *end,
              void *env) {
      (void)env;
      switch (n ++) {
      case 0:
        return bc_dct_put("id", cstr, ptr, end, "mnopqrstuvwxyz123456");
      case 1:
        return ptr;
      }
      return NULL;
    }

    char *end = krpc_put(buf, endof(buf), &msg, put);
    assert(n == 2);
    assert(match(buf, end, "d1:rd2:id20:mnopqrstuvwxyz123456e1:t2:aa1:y1:re"));
  }
  
  test_done();
}
