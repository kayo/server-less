import "mocha";
import * as assert from "assert";
import { bc_ref, bc_ref_get, bc_get_str, bc_dct_put_str } from "../src/bcoder";
import { krpc_msg, krpc_data_none, krpc_data_error, krpc_data_query, krpc_data_response, krpc_get, krpc_put } from "../src/krpc";

const key_id = Buffer.from("id");
const str_aa = Buffer.from("aa");
const str_ping = Buffer.from("ping");

function match(buf: Buffer, end: Buffer | null, str: string): void {
  assert(end != null);
  assert.equal(buf.toString('ascii', 0, buf.length - (end as Buffer).length), str);
}

describe("KRPC tests", () => {
  describe("Error message", () => {
    const str = "d1:eli201e23:A Generic Error Ocurrede1:t2:aa1:y1:ee";

    it("decode", () => {
      const buf = Buffer.from(str, 'ascii');
      const msg = krpc_data_none<void>(undefined);
      const res = krpc_get(buf, msg);

      assert.notEqual(res, null);
      assert.equal((res as Buffer).length, 0);
      assert.equal(msg.type, krpc_msg.error);
      assert.deepEqual(msg.tid, str_aa);
      assert.equal(msg.code, 201);
      assert.equal(msg.msg, "A Generic Error Ocurred");
    });

    it("encode", () => {
      const buf = Buffer.alloc(64);
      const msg = krpc_data_error(str_aa, 201, "A Generic Error Ocurred");

      match(buf, krpc_put(buf, msg), str);
    });
  });

  describe("DHT ping", () => {
    const qstr = "d1:ad2:id20:abcdefghij0123456789e1:q4:ping1:t2:aa1:y1:qe";

    it("decode query", () => {
      const buf = Buffer.from(qstr);
      const msg = krpc_data_none<void>(undefined);

      let n = 0;
      const get = function(key: Buffer, buf: Buffer): Buffer | null {
        switch (n++) {
          case 0:
            assert.deepEqual(key, key_id);
            const val = bc_ref<Buffer>();
            const res = bc_get_str(buf, val);
            if (res == null) break;
            assert.deepEqual(bc_ref_get(val), Buffer.from("abcdefghij0123456789"));
            return res;
        }
        return null;
      };

      const res = krpc_get(buf, msg, get);

      assert.notEqual(res, null);
      assert.equal((res as Buffer).length, 0);
      assert.equal(msg.type, krpc_msg.query);
      assert.deepEqual(msg.tid, str_aa);
      assert.deepEqual(msg.query, str_ping);
      assert.equal(n, 1);
    });

    it("encode query", () => {
      const buf = Buffer.alloc(64);
      const msg = krpc_data_query<void>(str_aa, str_ping, undefined);

      let n = 0;
      const put = function(buf: Buffer): Buffer | null {
        switch (n++) {
          case 0:
            return bc_dct_put_str(key_id, buf, Buffer.from("abcdefghij0123456789"));
          case 1:
            return buf;
        }
        return null;
      }

      const res = krpc_put(buf, msg, put);
      assert.equal(n, 2);
      match(buf, res, qstr);
    });

    const rstr = "d1:rd2:id20:mnopqrstuvwxyz123456e1:t2:aa1:y1:re";

    it("decode response", () => {
      const buf = Buffer.from(rstr);
      const msg = krpc_data_none<void>(undefined);

      let n = 0;
      const get = function(key: Buffer, buf: Buffer) {
        switch (n++) {
          case 0:
            assert.deepEqual(key, key_id);
            const val = bc_ref<Buffer>();
            const res = bc_get_str(buf, val);
            if (res == null) break;
            assert.deepEqual(bc_ref_get(val), Buffer.from("mnopqrstuvwxyz123456"));
            return res;
        }
        return null;
      }

      const res = krpc_get(buf, msg, get);
      assert.notEqual(res, null);
      assert.equal((res as Buffer).length, 0);
      assert.equal(msg.type, krpc_msg.response);
      assert.deepEqual(msg.tid, str_aa);
      assert.equal(n, 1);
    });

    it("encode response", () => {
      const buf = Buffer.alloc(64);
      const msg = krpc_data_response<void>(str_aa, undefined);

      let n = 0;
      const put = function(buf: Buffer): Buffer | null {
        switch (n++) {
          case 0:
            return bc_dct_put_str(key_id, buf, Buffer.from("mnopqrstuvwxyz123456"));
          case 1:
            return buf;
        }
        return null;
      }

      const res = krpc_put(buf, msg, put);
      assert.equal(n, 2);
      match(buf, res, rstr);
    });
  });

  describe("DHT find_node", () => {
    it("decode", () => {

    });

    it("encode", () => {

    });
  });

  describe("DHT get_peers", () => {
    it("decode", () => {

    });

    it("encode", () => {

    });
  });

  describe("DHT announce_peer", () => {
    it("decode", () => {

    });

    it("encode", () => {

    });
  });
});
