#ifndef __TEST_H__
#define __TEST_H__

#include <stdio.h>

#define info(t) fprintf(stderr, "\e[1;37m.... ??\e[0m \e[1;34m%s\e[0m ", t)
#define pass()  fprintf(stderr, "\r\e[1;32mPASS (:\e[0m\n")
#define fail(f, ...) fprintf(stderr, "\r\e[1;31mFAIL ):\e[0m\n" f "\n", ##__VA_ARGS__)

#define test_main()         \
  int main(void)

#define test_init()         \
  int ret_code = 0

#define test_done()         \
  return ret_code

#define assert(c, ...) {    \
    info(#c);               \
    if (c) {                \
      pass();               \
    } else {                \
      ret_code ++;          \
      fail("%s:%d",         \
           __FILE__,        \
           __LINE__);       \
    }                       \
  }

#define test(t, c, ...) {   \
    info(t);                \
    if (c) {                \
      pass();               \
    } else {                \
      ret_code ++;          \
      fail(#c);             \
    }                       \
  }

#endif /* __TEST_H__ */
